package com.gar.utils;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.gar.R;
import com.gar.models.QuestionClass;
import com.gar.views.activities.LoginActivity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class Util {

    /* REQUEST_CODE for android 6.0 gps permission */
    public static final int REQUEST_PERMISSION_CODE_LOCATION = 255;
    public static final int REQUEST_PERMISSION_CODE_CAMERA = 265;
    public static final int REQUEST_PERMISSION_CODE_EXTERNAL_STORAGE = 275;


    public static void logout(final Activity activity, final Context context){
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.alert_logout);

        ImageView image = (ImageView) dialog.findViewById(R.id.alert_logout);
        image.setImageResource(R.drawable.ic_alert_logout);

        TextView head = (TextView) dialog.findViewById(R.id.alert_logout_title);
        head.setText("Salir");
        TextView content = (TextView) dialog.findViewById(R.id.alert_logout_content);
        content.setText("¿Está seguro de cerrar sesión?");

        Button dialogButtonOk = (Button) dialog.findViewById(R.id.alert_ok);
        dialogButtonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent login=new Intent(activity, LoginActivity.class);
                login.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                activity.startActivity(login);
                activity.finish();
            }
        });
        Button dialogButtonCancel = (Button) dialog.findViewById(R.id.alert_cancel);
        dialogButtonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public static void info(final Context context, String title, String contents){
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.alert_info);

        ImageView image = (ImageView) dialog.findViewById(R.id.alert_info);
        image.setImageResource(R.drawable.ic_alert_info);

        TextView head = (TextView) dialog.findViewById(R.id.alert_info_title);
        head.setText(title);
        TextView content = (TextView) dialog.findViewById(R.id.alert_info_content);
        content.setText(contents);

        Button dialogButtonOk = (Button) dialog.findViewById(R.id.alert_ok);
        dialogButtonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        Button dialogButtonCancel = (Button) dialog.findViewById(R.id.alert_cancel);
        dialogButtonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }


    public static String getDateFromCode(String str){
        SimpleDateFormat spanish = new SimpleDateFormat("EEEE d 'de' MMMM 'del' yyyy", new Locale("es", "ES"));
        String date = str.substring(0, str.indexOf("-")); //Tienes la fecha
        date = str.substring(0,2) + "/" + str.substring(2,4) + "/" + str.substring(4,8);
        Date thatDay = null;
        try {
            thatDay = new SimpleDateFormat("dd/MM/yyyy").parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String fecha = spanish.format(thatDay);
        return fecha;
    }

    public static void getClassesList(Activity activity, ListView itemListView) {
        ArrayList<String> classes = new ArrayList<String>();
        classes.add("Evaluaciones");classes.add("Preguntas");
        ArrayAdapter<String> adapter_multiple_choice = new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_multiple_choice, classes);
        itemListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        itemListView.setAdapter(adapter_multiple_choice);

    }
    public static void enableLocation(Activity activity, Context context) {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_PERMISSION_CODE_LOCATION);
            return;
        }
    }
    public static void enableCamera(Activity activity, Context context) {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CAMERA}, REQUEST_PERMISSION_CODE_CAMERA);
            return;
        }
    }
    public static void enableExternalStorage(Activity activity, Context context){
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_PERMISSION_CODE_EXTERNAL_STORAGE);
            return;
        }
    }

}
