package com.gar.config;

public class ConstValue {

	public static String MAIN_PREF = "GARITA";
	public static String SITE_URL = "https://digital.siderperu.pe:443/api";


	/* GET DATA */
	public static String JSON_LOGIN = SITE_URL+"";
	public static String JSON_EVALUATION = SITE_URL + "/sentry/evaluation/garita/";
	public static String JSON_QUESTION = SITE_URL + "/sentry/question/";

	/* SEND DATA */
	public static String JSON_OT = SITE_URL+"/programming/ot_return/";
	public static String JSON_SEND_RESULT = SITE_URL + "/sentry/evaluation/result/";
	public static String JSON_SEND_DECLINE = SITE_URL + "/sepa/decline/";
	public static String JSON_SEND_IMAGE = SITE_URL + "/sentry/evaluation/photo/";
	public static String JSON_SEND_OT_REGISTRO = SITE_URL + "sentry/statusOTransport/";

	//@GeneralUser
	public static String userId;
	public static String getUserId() {
		return userId;
	}
	public static void setUserId(String userId) {
		ConstValue.userId = userId;
	}

	public static String company;
    public static String getUserCompany() { return company; } public static void setUserCompany(String company) { ConstValue.company = company; }

	public static String name;

	public static String getName() {
		return name;
	}

	public static void setName(String name) {
		ConstValue.name = name;
	}

	public static String documentType;

	public static String getDocumentType() {
		return documentType;
	}

	public static void setDocumentType(String documentType) {
		ConstValue.documentType = documentType;
	}

	public static String document;

	public static String getDocument() {
		return document;
	}

	public static void setDocument(String document) {
		ConstValue.document = document;
	}

	public static String photoName;

	public static String getPhotoName() {
		return photoName;
	}

	public static void setPhotoName(String photoName) {
		ConstValue.photoName = photoName;
	}

	//@QRCode
	public static String resultQR = "";
	public static String getResultQR() { return resultQR; } public static void setResultQR(String resultQR) { ConstValue.resultQR = resultQR; }

	//@GeneralUriImage
	public static String uri;
	public static String getUri() { return uri; } public static void setUri(String uri) { ConstValue.uri = uri; }


	public static String evaluationName;
	public static String getEvaluationName() {
		return evaluationName;
	}

	public static void setEvaluationName(String evaluationName) {
		ConstValue.evaluationName = evaluationName;
	}

	//@GeneraImagePath
	public static String path;
	public static String getPath() { return path; } public static void setPath(String path) { ConstValue.path = path; }

	public static String namePhoto;
	public static String getNamePhoto() { return namePhoto; } public static void setNamePhoto(String namePhoto) { ConstValue.namePhoto = namePhoto; }

	public static String evaluationId;
	public static String getEvaluationId() { return evaluationId; } public static void setEvaluationId(String evaluationId) { ConstValue.evaluationId = evaluationId; }

	/**
	 * Sync Values
	 */
	public static String checkedEvaluations;

	public static String getCheckedEvaluations() {
		return checkedEvaluations;
	}

	public static void setCheckedEvaluations(String checkedEvaluations) {
		ConstValue.checkedEvaluations = checkedEvaluations;
	}

	public static String checkedQuestions;

	public static String getCheckedQuestions() {
		return checkedQuestions;
	}

	public static void setCheckedQuestions(String checkedQuestions) {
		ConstValue.checkedQuestions = checkedQuestions;
	}
}
