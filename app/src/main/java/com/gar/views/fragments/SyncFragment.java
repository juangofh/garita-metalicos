package com.gar.views.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gar.R;
import com.gar.adapters.GeneralSpinnerAdapter;
import com.gar.adapters.SynchronizationAdapter;
import com.gar.config.ConstValue;
import com.gar.db.SqliteClass;
import com.gar.models.EvaluationClass;
import com.gar.models.QuestionClass;
import com.gar.models.SynchronizationClass;
import com.gar.utils.Common;
import com.gar.utils.JSONParser;
import com.gar.utils.Util;
import com.gar.views.activities.MainActivity;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class SyncFragment extends Fragment {

    Activity activity;
    Context context;
    Common common;
    static ArrayList<HashMap<String, String>> data;
    ListView listView;
    ArrayList<String> syncOptions;
    ProgressDialog dialog;
    Spinner spSyncOptions;
    View rootView;
    ListView itemListView;
    List<SynchronizationClass> itemList;

    boolean allChecked = false;

    private Menu menu;

    private EvaluationClass Evaluation; private ArrayList<EvaluationClass> loadEvaluation;
    private QuestionClass Question; private ArrayList<QuestionClass> loadQuestion;

    BottomSheetDialog bottomSheetDialog;

    private static final String FRAGMENT_NAME = "SyncFragment";

    public SyncFragment() {}


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        context = getActivity();
        syncOptions = new ArrayList<String>();
        syncOptions.add("SINCRONIZADAS"); syncOptions.add("SINCRONIZAR");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_sync, container, false);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Sincronizar");
        common = new Common();
        activity = getActivity();
        context = getActivity();


        data = new ArrayList<HashMap<String, String>>();
        itemListView = (ListView) rootView.findViewById(R.id.lst_classes_sync);
        itemList = SqliteClass.getInstance(context).databasehelp.synchronizationsql.getSynchronizationData();
        final TextView emptyTextView = (TextView) rootView.findViewById(android.R.id.empty);

        spSyncOptions = (Spinner) rootView.findViewById(R.id.sp_sync_options);
        spSyncOptions.setAdapter(new GeneralSpinnerAdapter(context, R.layout.spinner_row, syncOptions, FRAGMENT_NAME));
        spSyncOptions.getBackground().setColorFilter(getResources().getColor(R.color.colorWhite), PorterDuff.Mode.SRC_ATOP);


        spSyncOptions.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> spn, android.view.View v, int position, long id) {
                switch (position) {
                    case 0:
                        hideOption(R.id.action_display);
                        itemList = SqliteClass.getInstance(context).databasehelp.synchronizationsql.getSynchronizationData();
                        getList();
                        if (itemList.size() > 0) emptyTextView.setVisibility(View.GONE);
                        else emptyTextView.setVisibility(View.VISIBLE);
                        break;
                    case 1:
                        showOption(R.id.action_display);
                        Util.getClassesList(activity, itemListView);
                        emptyTextView.setVisibility(View.GONE);
                        break;
                    default:
                        break;
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        bottomSheetDialog = new BottomSheetDialog(activity);
        View sheetDialog = getActivity().getLayoutInflater().inflate(R.layout.dialog_bottom_sheet, null);
        bottomSheetDialog.setContentView(sheetDialog);

        View chooseAllView = sheetDialog.findViewById(R.id.action_choose_all);
        chooseAllView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("CANTIDAD DE HIJOS " + itemListView.getChildCount());
                if (allChecked) {
                    for (int i = 0; i < itemListView.getChildCount(); i++) {
                        itemListView.setItemChecked(i, false);
                    }
                    allChecked = false;
                } else {
                    for (int i = 0; i < itemListView.getChildCount(); i++) {
                        itemListView.setItemChecked(i, true);
                    }
                    allChecked = true;
                }
            }
        });

        View synAllView = sheetDialog.findViewById(R.id.action_sync_all);
        synAllView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (itemListView.getCheckedItemCount() > 0) {
                    final SparseBooleanArray sparseBooleanArray = itemListView.getCheckedItemPositions();

                    if (sparseBooleanArray.get(0)) { ConstValue.setCheckedEvaluations("1"); }
                    else { ConstValue.setCheckedEvaluations("0"); }
                    if (sparseBooleanArray.get(1)) { ConstValue.setCheckedQuestions("1"); }
                    else { ConstValue.setCheckedQuestions("0"); }

                    String message = "";
                    if (sparseBooleanArray.get(0)) message += "\n - \tEvaluaciones";
                    if (sparseBooleanArray.get(1)) message += "\n - \tPreguntas";

                    final Dialog dialog = new Dialog(activity);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.alert_info);
                    TextView head = (TextView) dialog.findViewById(R.id.alert_info_title);
                    head.setText("CloudSSP - Sincronizar Información");
                    TextView content = (TextView) dialog.findViewById(R.id.alert_info_content);

                    content.setText("¿Esta seguro de descargar nueva información del servidor? \n" +
                            "Se descargaran los siguientes datos: \n" + message);

                    Button dbOk = (Button) dialog.findViewById(R.id.alert_ok);
                    dbOk.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            new loadTask().execute(true);
                            dialog.dismiss();
                        }
                    });
                    Button dbCancel = (Button) dialog.findViewById(R.id.alert_cancel);
                    dbCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    dialog.show();
                }
                else {
                    Toast.makeText(context, "Seleccione alguna opción para sincronizar.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return rootView;
    }

    public void getList() {
        data = new ArrayList<HashMap<String, String>>();
        itemListView.setChoiceMode(ListView.CHOICE_MODE_NONE);
        for (int z = 0; z < itemList.size(); z++) {
            SynchronizationClass cc = itemList.get(z);

            HashMap<String, String> map = new HashMap<String, String>();
            map.put("id", cc.getId() + "");
            map.put("class_name", cc.getClassName());
            map.put("date_hour_start", cc.getDateHourStart());
            map.put("date_hour_end", cc.getDateHourEnd());
            map.put("registers", cc.getRegisterNumber());
            map.put("failures", cc.getFailureNumber());
            map.put("state", cc.getState());
            data.add(map);

        }
        SynchronizationAdapter adapter = new SynchronizationAdapter(activity, data);
        itemListView.setAdapter(adapter);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        try {
            this.menu = menu;
            inflater.inflate(R.menu.menu_action_sheet, menu);
            super.onCreateOptionsMenu(menu, inflater);
        }
        catch (Exception e){
            e.printStackTrace();
        }
        finally {
            hideOption(R.id.action_display);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_logout) {
            Util.logout(getActivity(), context);
        }
        else if(id == R.id.action_display){
            bottomSheetDialog.show();
        }

        return super.onOptionsItemSelected(item);
    }

    private void hideOption(int id) {
        MenuItem item = menu.findItem(id);
        item.setVisible(false);
    }
    private void showOption(int id){
        MenuItem item = menu.findItem(id);
        item.setVisible(true);
    }
    @Override
    public void onDetach() { super.onDetach(); }


    class loadTask extends AsyncTask<Boolean, Void, String> {

        @Override
        protected void onPreExecute() {
            dialog = ProgressDialog.show(activity, "", "Sincronizando, espere por favor...", true);
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
                Toast.makeText(activity, "CloudSSP " + result, Toast.LENGTH_LONG).show();
            } else {

                final Dialog dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.alert_info);
                ImageView image = (ImageView) dialog.findViewById(R.id.alert_info);
                image.setImageResource(R.drawable.ic_alert_info);
                TextView head = (TextView) dialog.findViewById(R.id.alert_info_title);
                head.setText("Sincronización");
                TextView content = (TextView) dialog.findViewById(R.id.alert_info_content);
                content.setText("La sincronización fue exitosa.");
                Button dbOk = (Button) dialog.findViewById(R.id.alert_ok);
                dbOk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, MainActivity.class);
                        intent.putExtra("fragment_position", 2);
                        startActivity(intent);
                        dialog.dismiss();
                    }
                });
                Button dbCancel = (Button) dialog.findViewById(R.id.alert_cancel);
                dbCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();

            }
            // TODO Auto-generated method stub
            dialog.dismiss();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            // TODO Auto-generated method stub
            super.onProgressUpdate(values);
        }

        @Override
        protected void onCancelled() {
            // TODO Auto-generated method stub
            super.onCancelled();
        }

        @Override
        protected void onCancelled(String result) {
            // TODO Auto-generated method stub
            super.onCancelled(result);
        }

        @Override
        protected String doInBackground(Boolean... params) {

            String responseString = null;
            SimpleDateFormat dateFormatHour = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());
            SimpleDateFormat dateFormatDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            Date date = new Date();
            String _date_sta = dateFormatDate.format(date);
            String _hour_sta = dateFormatHour.format(date);

            try {
                JSONParser jParser;
                JSONObject json;
                String url = "";
                if (ConstValue.getCheckedEvaluations().equals("1")) {
                    loadEvaluation = new ArrayList<EvaluationClass>();
                    jParser = new JSONParser();
                    url = ConstValue.JSON_EVALUATION;
                    json = jParser.getJSONFromUrl(url);
                    System.out.println("MI JSON EVALUATION: " + json);
                    if (json.has("data")) {
                        if (json.get("data") instanceof JSONArray) {
                            JSONArray jsonList = json.getJSONArray("data");
                            int correctRegister = 0;
                            int failureRegister = 0;
                            for (int i = 0; i < jsonList.length(); i++) {
                                JSONObject o = jsonList.getJSONObject(i);
                                int idComing = o.getInt("id");
                                boolean exists = SqliteClass.getInstance(context).databasehelp.evaluationsql.checkIfExists(String.valueOf(idComing));
                                try {
                                    if (exists){
                                        Log.i("Evaluation","Update");
                                        Evaluation =  new EvaluationClass(o.getInt("id"), o.getString("name"), o.getString("startDate"),
                                                o.getString("endDate"), o.getString("app"), o.getString("link"), o.getString("type"), o.getString("hour"), o.getString("limit"), o.getString("active"));
                                        loadEvaluation.add(Evaluation);
                                        SqliteClass.getInstance(context).databasehelp.evaluationsql.updateEvaluation(Evaluation, String.valueOf(idComing));
                                    }
                                    else {
                                        Log.i("Evaluation","Create and save in DB");
                                        Evaluation =  new EvaluationClass(o.getInt("id"), o.getString("name"), o.getString("startDate"),
                                                o.getString("endDate"), o.getString("app"), o.getString("link"), o.getString("type"), o.getString("hour"), o.getString("limit"), o.getString("active"));
                                        loadEvaluation.add(Evaluation);
                                        SqliteClass.getInstance(context).databasehelp.evaluationsql.addEvaluation(Evaluation);

                                    }
                                    correctRegister++;
                                } catch (Exception e) {
                                    responseString = e.toString();
                                    e.printStackTrace();
                                    failureRegister++;
                                }
                            }
                            Date _date = new Date();
                            String _date_end = dateFormatDate.format(_date);
                            String _hour_end = dateFormatHour.format(_date);
                            /** Crear la sincronizacion */
                            String state = "1";
                            if (failureRegister > 0) {
                                state = "0";
                            }
                            SynchronizationClass synchronizationClass = new SynchronizationClass(0, "Evaluaciones", _date_sta + " " + _hour_sta,
                                    _date_end + " " + _hour_end, correctRegister + "", failureRegister + "", state);
                            SqliteClass.getInstance(context).databasehelp.synchronizationsql.addSynchronization(synchronizationClass);
                        }
                    }
                }

                if(ConstValue.getCheckedQuestions().equals("1")){
                    loadQuestion = new ArrayList<QuestionClass>();
                    jParser = new JSONParser();
                    url = ConstValue.JSON_QUESTION;
                    json = jParser.getJSONFromUrl(url);
                    if (json.has("data")) {
                        if(json.get("data") instanceof JSONArray){
                            JSONArray jsonList = json.getJSONArray("data");
                            int correctRegister = 0;
                            int failureRegister = 0;
                            for (int i = 0; i < jsonList.length(); i++) {
                                JSONObject o = jsonList.getJSONObject(i);
                                int idComing = o.getInt("id");
                                boolean exists = SqliteClass.getInstance(context).databasehelp.questionsql.checkIfExists(String.valueOf(idComing));
                                try{
                                    if(exists){
                                        Log.i("Department","Update"); /** No hay problema que se actualice con 0.0 porque no se pueden tocar hasta que se cierre la supervision */
                                        Question =  new QuestionClass(o.getInt("id"), o.getString("name"), o.getString("evaluationID"), o.getString("order"), o.getString("type"), o.getString("parameters"), o.getString("questionAlternatives"),o.getString("active"));
                                        loadQuestion.add(Question);
                                        SqliteClass.getInstance(context).databasehelp.questionsql.updateQuestion(Question, String.valueOf(idComing));
                                    }
                                    else{
                                        Log.i("Department","Create and save in DB");
                                        Question =  new QuestionClass(o.getInt("id"), o.getString("name"), o.getString("evaluationID"), o.getString("order"), o.getString("type"), o.getString("parameters"), o.getString("questionAlternatives"),o.getString("active"));
                                        loadQuestion.add(Question);
                                        SqliteClass.getInstance(context).databasehelp.questionsql.addQuestion(Question);
                                    }
                                    correctRegister++;
                                }
                                catch (Exception e){
                                    responseString = e.toString();
                                    e.printStackTrace();
                                    failureRegister++;
                                }
                            }
                            Date _date = new Date();
                            String _date_end = dateFormatDate.format(_date);
                            String _hour_end = dateFormatHour.format(_date);
                            /** Crear la sincronizacion */
                            String state = "1";
                            if (failureRegister > 0) {
                                state = "0";
                            }
                            SynchronizationClass synchronizationClass = new SynchronizationClass(0, "Preguntas", _date_sta + " " + _hour_sta,
                                    _date_end + " " + _hour_end, correctRegister + "", failureRegister + "", state);
                            SqliteClass.getInstance(context).databasehelp.synchronizationsql.addSynchronization(synchronizationClass);
                        }

                    }
                }
            } catch (Exception e) {
                // TODO: handle exception
                responseString = e.toString();
                e.printStackTrace();
                responseString = "CloudSSP - Error al recuperar la información del portal.";
            }
            // TODO Auto-generated method stub
            return responseString;
        }
    }
}
