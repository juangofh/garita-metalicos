package com.gar.views.fragments;


import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.le.ScanRecord;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.app.Fragment;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.gar.R;
import com.gar.config.ConstValue;
import com.gar.db.SqliteClass;
import com.gar.models.OTClass;
import com.gar.utils.Common;
import com.gar.views.activities.CheckListActivity;
import com.gar.views.activities.ScanQRActivity;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.journeyapps.barcodescanner.BarcodeEncoder;

public class HomeFragment extends Fragment implements  MenuItem.OnActionExpandListener {

    View convertView;
    Activity activity;
    Context context;
    Common common;

    ImageView takeQR;
    ScrollView informationFrame;
    TextView ot, labelTake;

    boolean isFABOpen = false;

    FloatingActionButton fabButton;
    TextView txMakeCalification;
    FrameLayout frameCalification;
    FloatingActionButton addCalification;

    TextView inputName, inputLicense, inputPlate, inputVehicleType;

    public HomeFragment(){}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        convertView = inflater.inflate(R.layout.fragment_home, container, false);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Garita");

        context = getActivity();
        activity = getActivity();
        common = new Common();

        informationFrame = (ScrollView) convertView.findViewById(R.id.sv_information);
        ot = (TextView)convertView.findViewById(R.id.tx_input_ot);
        labelTake = (TextView) convertView.findViewById(R.id.tx_label_take);


        takeQR = (ImageView) convertView.findViewById(R.id.ic_take_qr);
        takeQR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, ScanQRActivity.class));
            }
        });

//        txReject = (TextView) convertView.findViewById(R.id.tx_reject);
//        txReject.setVisibility(View.GONE);
//        frameReject = (FrameLayout) convertView.findViewById(R.id.frame_reject);

        txMakeCalification = (TextView) convertView.findViewById(R.id.tx_calification);
        txMakeCalification.setVisibility(View.GONE);
        frameCalification = (FrameLayout) convertView.findViewById(R.id.frame_calification);

        addCalification = (FloatingActionButton) convertView.findViewById(R.id.action_calification);
//        makeReject = (FloatingActionButton) convertView.findViewById(R.id.action_reject);
        fabButton = (FloatingActionButton) convertView.findViewById(R.id.action_initial);

        fabButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isFABOpen){ showFABMenu(); }
                else{ closeFABMenu(); }
            }
        });

        addCalification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeFABMenu();
                Intent intent = new Intent(context, CheckListActivity.class);
                startActivity(intent);
            }
        });


//        makeReject.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(context, "Send service and cancel QR", Toast.LENGTH_SHORT).show();
//                closeFABMenu();
//            }
//        });

        inputName = (TextView) convertView.findViewById(R.id.tx_input_name);
        inputLicense = (TextView) convertView.findViewById(R.id.tx_input_license);
        inputPlate = (TextView) convertView.findViewById(R.id.tx_input_plate);
        inputVehicleType = (TextView) convertView.findViewById(R.id.tx_input_vehicle_type);

        Log.e("BUG2","" + ConstValue.getResultQR());
        if(ConstValue.getResultQR().equals("")){ disableViews(); }
        else{
            enableViews();

            OTClass otClass = SqliteClass.getInstance(context).databasehelp.otsql.getOT(ConstValue.getResultQR());
            inputName.setText(otClass.getLastname() + ", " + otClass.getFirstname());
            inputLicense.setText(otClass.getLicense());
            inputPlate.setText(otClass.getPlate());
            inputVehicleType.setText(otClass.getTypeVehicle());

            //TODO hacer saber que el diseño esta mal, debe mandar id de evaluacion y ya yo sabre que tipo de servicio es
            /**
             * Entonces aqui tendria que setear el id de la evaluacion correspondiente!
             */
            ConstValue.setEvaluationId(otClass.getEvaluationID());
            System.out.println("Gaaaa" + otClass.getEvaluationID());

        }
        return convertView;
    }

    private void enableViews(){
        fabButton.setVisibility(View.VISIBLE);
        addCalification.setVisibility(View.VISIBLE);
//        makeReject.setVisibility(View.VISIBLE);
        labelTake.setVisibility(View.GONE);
        informationFrame.setVisibility(View.VISIBLE);
        ot.setText(ConstValue.getResultQR());
        try {
            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
            Bitmap bitmap = barcodeEncoder.encodeBitmap(ConstValue.getResultQR(), BarcodeFormat.QR_CODE, 400, 400);
            takeQR.setImageBitmap(bitmap);
        }
        catch (WriterException e) {
            e.printStackTrace();
        }
    }
    private void disableViews(){
        labelTake.setVisibility(View.VISIBLE);
        informationFrame.setVisibility(View.GONE);
        fabButton.setVisibility(View.GONE);
        addCalification.setVisibility(View.GONE);
//        makeReject.setVisibility(View.GONE);
    }

    private void showFABMenu(){
        isFABOpen=true;
//        frameReject.animate().translationY(-getResources().getDimension(R.dimen.standard_55));
//        txReject.setVisibility(View.VISIBLE);
        frameCalification.animate().translationY(-getResources().getDimension(R.dimen.standard_55));
        txMakeCalification.setVisibility(View.VISIBLE);
    }
    private void closeFABMenu(){
        isFABOpen=false;
//        frameReject.animate().translationY(0);
//        txReject.setVisibility(View.GONE);
        frameCalification.animate().translationY(0);
        txMakeCalification.setVisibility(View.GONE);
    }



    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_home, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
//        int id = item.getItemId();
//        if (id == R.id.action_sync) {}
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onMenuItemActionExpand(MenuItem item) {
        return true;
    }

    @Override
    public boolean onMenuItemActionCollapse(MenuItem item) {
        return true;
    }


}