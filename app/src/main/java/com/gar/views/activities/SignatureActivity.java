package com.gar.views.activities;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.github.gcacace.signaturepad.views.SignaturePad;
import com.gar.R;
import com.gar.config.ConstValue;
import com.gar.db.SqliteClass;
import com.gar.models.LoadImageClass;
import com.gar.models.ResultClass;
import com.gar.utils.Common;
import com.gar.utils.Util;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class SignatureActivity extends AppCompatActivity {

    Context context;
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {Manifest.permission.WRITE_EXTERNAL_STORAGE};
    private SignaturePad mSignaturePad;
    private Button mClearButton;
    private Button mSaveButton;
    String mCurrentPhotoPath;
    String imageFileName = "";

    ProgressDialog dialogOTClosing;
    ArrayList<ResultClass> pendingResults;
    ArrayList<LoadImageClass> loadImage;
    Common common;

    private String _date, _hour;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    private SimpleDateFormat hourFormat = new SimpleDateFormat("HH:mm:ss");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signature);
        verifyStoragePermissions(this);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        context = this;
        mClearButton = (Button) findViewById(R.id.clear_button);
        mClearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSignaturePad.clear();
            }
        });
        mSaveButton = (Button) findViewById(R.id.save_button);
        mSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bitmap signatureBitmap = mSignaturePad.getSignatureBitmap();
                if (saveJpgSignature(signatureBitmap)) {
                    _date = dateFormat.format(new Date());
                    _hour = hourFormat.format(new Date());
                    LoadImageClass loadImage = new LoadImageClass(0, ConstValue.getUserId(), ConstValue.getEvaluationId(), ConstValue.getEvaluationName(),
                            ConstValue.getDocumentType(), ConstValue.getName(), ConstValue.getDocument(), _date, _hour,
                            "digital_signature", mCurrentPhotoPath, "", "0");
                    SqliteClass.getInstance(context).databasehelp.loadImagesql.addLoadImage(loadImage);
                    new sendResults().execute(true);
                } else {
                    Toast.makeText(context, "Error al guardar la firma.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        mSignaturePad = (SignaturePad) findViewById(R.id.signature_pad);
        mSignaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {

            }

            @Override
            public void onSigned() {
                mSaveButton.setEnabled(true);
                mClearButton.setEnabled(true);
            }

            @Override
            public void onClear() {
                mSaveButton.setEnabled(false);
                mClearButton.setEnabled(false);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            Intent intent = new Intent(context, MainActivity.class);
            startActivity(intent);
        } else if (id == R.id.action_logout) {
            Util.logout(this, context);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Util.logout(this, context);
    }

    public void saveBitmapToJPG(Bitmap bitmap, File photo) throws IOException {
        Bitmap newBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(newBitmap);
        canvas.drawColor(Color.WHITE);
        canvas.drawBitmap(bitmap, 0, 0, null);
        OutputStream stream = new FileOutputStream(photo);
        newBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
        stream.close();
    }

    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName, ".jpg", storageDir);
        mCurrentPhotoPath = image.getAbsolutePath();
        ConstValue.setPhotoName(image.getName());
        return image;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length <= 0 || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(context, "Error al guardar la firma.", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    public boolean saveJpgSignature(Bitmap signature) {
        boolean result = false;
        try {
            File photo = createImageFile();
            saveBitmapToJPG(signature, photo);
            scanMediaFile(photo);
            result = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    private void scanMediaFile(File photo) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.fromFile(photo);
        mediaScanIntent.setData(contentUri);
        SignatureActivity.this.sendBroadcast(mediaScanIntent);
    }

    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(activity, PERMISSIONS_STORAGE, REQUEST_EXTERNAL_STORAGE);
        }
    }

    class sendResults extends AsyncTask<Boolean, Void, String> {
        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            dialogOTClosing = ProgressDialog.show(context, "E Learning", getString(R.string.action_loading), true);
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            //startService(new Intent(context, ServicePM.class));
            if (result != null) {
                Toast.makeText(getApplicationContext(), "Error en el envío de datos: " + result, Toast.LENGTH_LONG).show();
            } else {
                new imageTask().execute(true);
            }
        }

        @Override
        protected String doInBackground(Boolean... params) {
            // TODO Auto-generated method stub
            String responseString = null;
            pendingResults = SqliteClass.getInstance(context).databasehelp.resultsql.getPendingResult();
            int total = pendingResults.size();
            try {
                JSONObject jsonobj;
                common = new Common();
                jsonobj = new JSONObject();
                jsonobj.put("total", String.valueOf(total));
                JSONArray results = new JSONArray();
                for (int i = 0; i < pendingResults.size(); i++) {
                    JSONObject singleResult = new JSONObject();
                    singleResult.put("app", "elearning");
                    singleResult.put("userCode", pendingResults.get(i).getUserCode());
                    singleResult.put("date", pendingResults.get(i).getDate());
                    singleResult.put("time", pendingResults.get(i).getHour());
                    singleResult.put("evaluationID", pendingResults.get(i).getEvaluationId());
                    String tmp = pendingResults.get(i).getDate().replace("-","");
                    singleResult.put("codeEvaluation", pendingResults.get(i).getEvaluationId() + tmp + pendingResults.get(i).getPersonalCode().replace(" ","") + pendingResults.get(i).getUserCode());
                    singleResult.put("evaluationName", pendingResults.get(i).getEvaluationName());
                    singleResult.put("questionID", pendingResults.get(i).getQuestionId());
                    singleResult.put("questionName", pendingResults.get(i).getQuestionName());
                    singleResult.put("personalCode", pendingResults.get(i).getPersonalCode());
                    singleResult.put("personalName", pendingResults.get(i).getPersonalName());
                    singleResult.put("personalDocument", pendingResults.get(i).getPersonalDocument());
                    singleResult.put("response", pendingResults.get(i).getAnswer());
                    results.put(singleResult);
                }
                jsonobj.put("detail", results);
                Log.i("JSON Result", jsonobj.toString());

                List<NameValuePair> nvps = new ArrayList<NameValuePair>();
                nvps.add(new BasicNameValuePair("data", jsonobj.toString()));
                JSONObject json = common.sendJsonData(ConstValue.JSON_SEND_RESULT, nvps);

                if (json.getString("response").equalsIgnoreCase("success")) {
                    Log.i("response", json.getString("response"));
                } else {
                    responseString = json.getString("response");
                }

            } catch (Exception e) {
                responseString = e.toString();
                Log.e("JSON Parser", "Error parsing data " + e.toString());
            }
            return responseString;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            // TODO Auto-generated method stub
            super.onProgressUpdate(values);
        }

        @Override
        protected void onCancelled() {
            // TODO Auto-generated method stub
            super.onCancelled();
        }

        @Override
        protected void onCancelled(String result) {
            // TODO Auto-generated method stub
            super.onCancelled(result);
        }
    }

    class imageTask extends AsyncTask<Boolean, Void, String> {
        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            dialogOTClosing.dismiss();
            SqliteClass.getInstance(context).databasehelp.resultsql.deleteResult();
            SqliteClass.getInstance(context).databasehelp.loadImagesql.deleteLoadImage();
            ConstValue.setPhotoName("");
            finish();
            Intent intent = new Intent(SignatureActivity.this, MainActivity.class);
            startActivity(intent);
            if (result.equals("")) {
                Toast.makeText(context, "Las preguntas e imágenes se enviaron exitosamente.", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(context, "Error en el envío de imágenes: " + result, Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected String doInBackground(Boolean... params) {
            // TODO Auto-generated method stub
            String responseString = "";
            loadImage = new ArrayList<LoadImageClass>();
            loadImage = SqliteClass.getInstance(context).databasehelp.loadImagesql.getPendingLoadImageData();
            int total = loadImage.size();
            try {
                JSONObject jsonobj;
                common = new Common();
                jsonobj = new JSONObject();
                jsonobj.put("total", String.valueOf(total));

                JSONArray results = new JSONArray();
                Log.e("BUG3","" + loadImage.size());
                for (int i = 0; i < loadImage.size(); i++) {
                    String file_path = loadImage.get(i).getPhoto();
                    File imageFile = new File(file_path);
                    if (imageFile.exists()) {
                        Bitmap bmInitial = BitmapFactory.decodeFile(file_path);
                        Bitmap bm = Bitmap.createScaledBitmap(bmInitial, 650, 400, false);
                        ByteArrayOutputStream bao = new ByteArrayOutputStream();
                        bm.compress(Bitmap.CompressFormat.JPEG, 90, bao);
                        byte[] ba = bao.toByteArray();
                        String imageEncode = Base64.encodeToString(ba, Base64.DEFAULT);
                        String imageName = ConstValue.getUserId() + "_" + loadImage.get(i).getPhoto() + ".jpg";

                        JSONObject singleResult = new JSONObject();

                        singleResult.put("id", loadImage.get(i).getId());
                        singleResult.put("codeLote", loadImage.get(i).getUserCode());
                        singleResult.put("app", "elearning");
                        singleResult.put("userCode", loadImage.get(i).getUserCode());
                        singleResult.put("date", loadImage.get(i).getDate());
                        singleResult.put("time", loadImage.get(i).getHour());
                        singleResult.put("evaluationID", loadImage.get(i).getEvaluationId());
                        singleResult.put("evaluationName", loadImage.get(i).getEvaluationName());
                        singleResult.put("personalCode", loadImage.get(i).getPersonalCode());
                        singleResult.put("personalName", loadImage.get(i).getPersonalName());
                        singleResult.put("personalDocument", loadImage.get(i).getPersonalDocument());
                        singleResult.put("namePhoto", ConstValue.getPhotoName());
                        singleResult.put("photo", imageEncode);
                        singleResult.put("observationPhoto", loadImage.get(i).getObservation());
                        results.put(singleResult);
                    }
                }
                jsonobj.put("detail", results);
                Log.i("JSON Image: ", jsonobj.toString());
                List<NameValuePair> nvps = new ArrayList<NameValuePair>();
                //nvps.add(new BasicNameValuePair("image", imageEncode));
                nvps.add(new BasicNameValuePair("data", jsonobj.toString()));
                Log.e("BUG3","" + "good until here");
                JSONObject jObj = common.sendJsonData(ConstValue.JSON_SEND_IMAGE, nvps);
                if (jObj.getString("response").equalsIgnoreCase("success")) {
                    //SqliteClass.getInstance(context).databasehelp.imagesql.loadImageData(String.valueOf(loadImage.get(x).getAi_id()));
                    //SqliteClass.getInstance(context).databasehelp.imagesql.deleteImageById(String.valueOf(loadImage.get(x).getAi_id()));
                    Log.i("response", jObj.getString("response"));
                } else {
                    responseString = jObj.getString("response");
                    Log.i("Image Response: ", jObj.getString("detail"));
                }

            } catch (JSONException e) {
                responseString = e.toString();
                Log.e("JSON Parser", "Error parsing data " + e.toString());
            }
            return responseString;
        }

        @Override
        protected void onCancelled() {
            // TODO Auto-generated method stub
            super.onCancelled();
        }

        @Override
        protected void onCancelled(String result) {
            // TODO Auto-generated method stub
            super.onCancelled(result);
        }

        private ArrayList<HashMap<String, String>> get_items() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            // TODO Auto-generated method stub
            super.onProgressUpdate(values);
        }
    }

}
