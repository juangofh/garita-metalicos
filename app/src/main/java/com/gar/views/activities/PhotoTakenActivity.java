package com.gar.views.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.gar.R;
import com.gar.config.ConstValue;
import com.gar.db.SqliteClass;
import com.gar.models.LoadImageClass;
import com.gar.models.OTClass;
import com.gar.utils.Util;
import com.github.chrisbanes.photoview.PhotoView;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class PhotoTakenActivity extends AppCompatActivity {

    Context context;
    EditText observation;
    FloatingActionButton saveImage;
    ImageView observationHelp;
    PhotoView photoView;
    OTClass otClass;

    private String _date, _hour;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    private SimpleDateFormat hourFormat = new SimpleDateFormat("HH:mm:ss");
    private LoadImageClass image;

    Uri uri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_taken);

        context = this;
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Guardar Foto");
        context = this;
        observation = (EditText) findViewById(R.id.et_observation);
        observationHelp = (ImageView) findViewById(R.id.ic_observation_help);
        observationHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "La observación es opcional.", Toast.LENGTH_SHORT).show();
            }
        });

        otClass = SqliteClass.getInstance(context).databasehelp.otsql.getOT(ConstValue.getResultQR());

        saveImage = (FloatingActionButton) findViewById(R.id.action_save);
        saveImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(saveImage()){
                    finish();
                    Intent intent = new Intent(PhotoTakenActivity.this, ResultActivity.class);
                    startActivity(intent);
                    Toast.makeText(context, "Su imagen ha sido guardada con éxito", Toast.LENGTH_SHORT).show();
                }
            }
        });
        photoView = (PhotoView) findViewById(R.id.ph_image_taken);
        uri = Uri.parse(String.valueOf(ConstValue.getUri()));
        Glide.with(context)
                .load(uri)
                .into(photoView);
    }

    public boolean saveImage(){
        try{
            _date =  dateFormat.format(new Date());
            _hour = hourFormat.format(new Date());
            image = new LoadImageClass((int)(Math.random()*10000), ConstValue.getUserId(), ConstValue.getEvaluationId(), ConstValue.getEvaluationName(),
                    "licencia", otClass.getLastname() + ", " + otClass.getFirstname(), otClass.getLicense(), _date, _hour,
                    ConstValue.getNamePhoto(), ConstValue.getPath(), observation.getText().toString(), "0");
            SqliteClass.getInstance(context).databasehelp.loadImagesql.addLoadImage(image);
            return true;
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return false;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        MenuItem menuItemCrop = menu.findItem(R.id.action_crop);
        menuItemCrop.setVisible(true);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == android.R.id.home){
            finish();
            Intent intent = new Intent(context, ResultActivity.class);
            startActivity(intent);
        }
        if(id==R.id.action_logout){
            Util.logout(PhotoTakenActivity.this, context);
        }
        if(id==R.id.action_crop){
            CropImage.activity(uri);
            uri = Uri.parse(String.valueOf(ConstValue.getUri()));
            CropImage.activity(uri).setBackgroundColor(getResources().getColor(R.color.colorBlack)).start(PhotoTakenActivity.this);
            Log.e("ACTION","Touched the crop button");
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Util.logout(PhotoTakenActivity.this, context);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), resultUri);
                    saveEditedImage(bitmap);
                } catch (Exception e) {
                    Toast.makeText(context, "Error al editar la imagen", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
                ConstValue.setUri(resultUri.toString());
                Intent intent = new Intent(this, PhotoTakenActivity.class);
                startActivity(intent);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }
    private void saveEditedImage(Bitmap bitmap) {
        File file = null;
        try {
            file = createImageFile();
        } catch (IOException e) {
            Log.e("CreateImageFile", "Error after attempting to create the container file for the image.");
            e.printStackTrace();
        }
        try {
            FileOutputStream outputStream = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream); //Como asociar un Uri con un OutputStream
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("Crop", "Failed to save");
            return;
        }
        Uri uri = FileProvider.getUriForFile(PhotoTakenActivity.this, "com.gar", file);
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("image/jpeg");
        intent.putExtra(Intent.EXTRA_STREAM, uri);
        startActivity(intent);
    }

    String mCurrentPhotoPath;
    String imageFileName="";

    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName, ".jpg", storageDir );

        mCurrentPhotoPath = image.getAbsolutePath();
        ConstValue.setNamePhoto(image.getName());
        ConstValue.setPath(mCurrentPhotoPath);
        return image;
    }
}
