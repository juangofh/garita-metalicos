package com.gar.views.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.gar.R;
import com.gar.config.ConstValue;
import com.gar.db.SqliteClass;
import com.gar.models.EvaluationClass;
import com.gar.models.QuestionClass;
import com.gar.models.UserClass;
import com.gar.utils.Common;
import com.gar.utils.ConnectionDetector;
import com.gar.utils.JSONParser;
import com.gar.utils.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Console;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class LoginActivity extends AppCompatActivity {

    public ConnectionDetector cd;
    ProgressDialog dialog;
    public String _date;
    Activity activity;
    Common common;
    Button btnLogin;
    EditText et_user;
    EditText et_pass;
    Context context;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

    public final static boolean DEVELOPMENT_MODE = true; /** Only in development to remove all database */


    private UserClass User; private ArrayList<UserClass> loadUser;
    private EvaluationClass Evaluation; private ArrayList<EvaluationClass> loadEvaluation;
    private QuestionClass Question; private ArrayList<QuestionClass> loadQuestion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        activity = this;
        cd = new ConnectionDetector(this);
        common = new Common();
        context = this;

        Util.enableLocation(activity, context);
        et_user = (EditText) findViewById(R.id.et_username);
        et_pass = (EditText) findViewById(R.id.et_password);
        Util.enableExternalStorage(LoginActivity.this, context);

        if(DEVELOPMENT_MODE){
            et_user.setText("jmoscoso");
            et_pass.setText("jmoscoso");
            SqliteClass.getInstance(context).databasehelp.deleteDataBase();
        }

        _date =  dateFormat.format(new Date());
        btnLogin = (Button)findViewById(R.id.button_login);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(et_user.getText().toString().length()==0){
                    et_user.setError("Por favor, ingrese usuario.");
                }else if (et_pass.getText().toString().length()==0){
                    et_pass.setError("Por favor, ingrese contraseña.");
                } else {
                    if(DEVELOPMENT_MODE){  SqliteClass.getInstance(context).databasehelp.deleteDataBase(); }
                    if(SqliteClass.getInstance(context).databasehelp.checkDataBase()){
                        String user = et_user.getText().toString();
                        String password = et_pass.getText().toString();
                        if (SqliteClass.getInstance(context).databasehelp.usersql.isRegisterUser(user, password)) {
                            String _dateUser = SqliteClass.getInstance(context).databasehelp.usersql.getData(1, et_user.getText().toString());
                            if (_date.equals(_dateUser)) {
                                if (SqliteClass.getInstance(context).databasehelp.usersql.isRegisterUser(et_user.getText().toString(), et_pass.getText().toString())) {
                                    //ConstValue.setUserCompany(SqliteClass.getInstance(context).databasehelp.usersql.getData(5, user));
                                    //Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                    //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                    //startActivity(intent);
                                    //finish();
                                    //TODO offline mode
                                }
                                else{
                                    Toast.makeText(getApplicationContext(), "Credenciales invalidas, intente nuevamente.", Toast.LENGTH_LONG).show();
                                }
                            }else{
                                SqliteClass.getInstance(context).databasehelp.deleteDataBase();
                                if(cd.isConnectingToInternet()){
                                    new loginTask().execute(true);
                                }else{
                                    Toast.makeText(getApplicationContext(), "Su dispositivo no cuenta con conexión a internet.", Toast.LENGTH_LONG).show();
                                }
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), "Usuario y/o contraseña incorrecta, porfavor intente nuevamente.", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        SqliteClass.getInstance(context).databasehelp.deleteDataBase();
                        if (cd.isConnectingToInternet()) {
                            new loginTask().execute(true);
                        } else {
                            Toast.makeText(getApplicationContext(), "Su dispositivo no cuenta con conexión a internet.", Toast.LENGTH_LONG).show();
                        }
                    }
                }
            }
        });

    }

    //TODO ir completando con los nuevos ConstValue que vaya agregando que sean necesarios
    public void offLine(String user){

        //SqliteClass.getInstance(context).databasehelp.usersql.updateActive("1");
        //Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        //startActivity(intent);
        //finish();
    }

    @Override
    public void onBackPressed() {
        activity.finish();
    }

    class loginTask extends AsyncTask<Boolean, Void, String> {
        String user, password;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            user = et_user.getText().toString();
            password = et_pass.getText().toString();
            dialog = ProgressDialog.show(LoginActivity.this, "", getString(R.string.action_loading), true);
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
                Toast.makeText(getApplicationContext(), "Error: " + result, Toast.LENGTH_LONG).show();
            } else {
                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
                finish();
            }
            // TODO Auto-generated method stub
            dialog.dismiss();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            // TODO Auto-generated method stub
            super.onProgressUpdate(values);
        }

        @Override
        protected void onCancelled() {
            // TODO Auto-generated method stub
            super.onCancelled();
        }

        @Override
        protected void onCancelled(String result) {
            // TODO Auto-generated method stub
            super.onCancelled(result);
        }

        @Override
        protected String doInBackground(Boolean... params) {
            String responseString = null;
            try {
                /*
                loadUser = new ArrayList<UserClass>();
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
                nameValuePairs.add(new BasicNameValuePair("username", user));
                nameValuePairs.add(new BasicNameValuePair("password", password));
                JSONObject jObj = common.sendJsonData(ConstValue.JSON_LOGIN, nameValuePairs);
                System.out.println("TU JSON User: " + jObj);

                try {
                    if (jObj.getString("response").equalsIgnoreCase("success")) {
                        JSONObject data = jObj.getJSONObject("data");
                        if (data.getString("user").equalsIgnoreCase(user)) {
                            //User = new UserClass(data.getInt("id"), user, password, data.getString("plate"),data.getString("code"),data.getString("email"),data.getString("fname"),data.getString("lname"),data.getString("dni"),data.getString("email"));
                            User = new UserClass(data.getInt("id"),user, password,data.getString("code"),data.getString("email"),data.getString("company"),data.getString("active"));
                            loadUser.add(User);
                            ConstValue.setUserId(String.valueOf(data.getInt("id")));
                            ConstValue.setUserCompany(data.getString("company"));
                            SqliteClass.getInstance(context).databasehelp.usersql.addUser(User);
                            //ConstValue.setUserIdInt(SqliteClass.getInstance(context).databasehelp.usersql.getId(user, password));
                        }
                    } else {
                        responseString = "Login - " + jObj.getString("data");
                        return responseString;
                    }
                } catch (JSONException e) {
                    responseString = "Login - " + e.getMessage();
                    return responseString;
                }
                */
                ConstValue.setUserId("10"); /** Only during test, this will be accomplished later by data we will get from upper service */
                JSONParser jParser;
                JSONObject json;
                String url = "";
                /*EVALUATION_CLASS*/
                loadEvaluation =  new ArrayList<EvaluationClass>();
                jParser = new JSONParser();
                url = ConstValue.JSON_EVALUATION;
                json = jParser.getJSONFromUrl(url);
                System.out.println("JSON EVALUATION: " + json);
                if (json.has("data")){
                    if (json.get("data")instanceof JSONArray){
                        JSONArray jsonList = json.getJSONArray("data");
                        for(int j=0;j<jsonList.length(); j++) {
                            JSONObject o = jsonList.getJSONObject(j);
                            Log.e("BUG1", "Encontro resultados " + o.toString());
                            Evaluation = new EvaluationClass(o.getInt("id"), o.getString("name"), o.getString("startDate"),
                                    o.getString("endDate"), o.getString("app"), o.getString("link"), o.getString("type"), o.getString("hour"), o.getString("limit"), o.getString("active"));
                            loadEvaluation.add(Evaluation);
                            SqliteClass.getInstance(context).databasehelp.evaluationsql.addEvaluation(Evaluation);
                        }
                    }
                }

                /*QUESTION_CLASS*/
                loadQuestion =  new ArrayList<QuestionClass>();
                jParser = new JSONParser();
                url = ConstValue.JSON_QUESTION;
                json = jParser.getJSONFromUrl(url);
                System.out.println("JSON QUESTION: " + json);
                if (json.has("data")){
                    if (json.get("data")instanceof JSONArray){
                        JSONArray jsonList = json.getJSONArray("data");
                        for(int j=0;j<jsonList.length(); j++){
                            JSONObject o =  jsonList.getJSONObject(j);
                            Question =  new QuestionClass(o.getInt("id"), o.getString("name"), o.getString("evaluationID"), o.getString("order"), o.getString("type"), o.getString("parameters"), o.getString("questionAlternatives"),o.getString("active"));
                            loadQuestion.add(Question);
                            SqliteClass.getInstance(context).databasehelp.questionsql.addQuestion(Question);
                        }
                    }
                }
            } catch (Exception e) {
                // TODO: handle exception
                e.printStackTrace();
                responseString = "Error al recuperar la información del portal.";
            }
            // TODO Auto-generated method stub
            return responseString;
        }
    }

}

