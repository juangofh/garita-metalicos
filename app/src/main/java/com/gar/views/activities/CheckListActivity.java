package com.gar.views.activities;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import com.gar.R;
import com.gar.adapters.CheckListAdapter;
import com.gar.config.ConstValue;
import com.gar.db.SqliteClass;
import com.gar.models.EvaluationClass;
import com.gar.models.LoadImageClass;
import com.gar.models.OTClass;
import com.gar.models.QuestionClass;
import com.gar.models.ResultClass;
import com.gar.utils.Common;
import com.gar.utils.JSONParser;
import com.gar.utils.Util;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class CheckListActivity extends AppCompatActivity {

    Context context;
    ListView listView;
    ArrayList<QuestionClass> itemList;
    static ArrayList<HashMap<String, String>> data;
    TextView empty;
    SimpleDateFormat dateFormatHour = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());
    SimpleDateFormat dateFormatDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
    EvaluationClass evaluationClass;
    OTClass otClass;
    ProgressDialog dialogOTClosing, dataDialog;

    ArrayList<ResultClass> pendingResults;
    ArrayList<LoadImageClass> loadImage;
    Common common;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_list);

        context = this;
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("CheckList");

        otClass = SqliteClass.getInstance(context).databasehelp.otsql.getOT(ConstValue.getResultQR());
        try{
            Log.e("Gaaaa - ", ConstValue.getEvaluationId());
            evaluationClass = SqliteClass.getInstance(context).databasehelp.evaluationsql.getEvaluation(ConstValue.getEvaluationId());
            ConstValue.setEvaluationName(evaluationClass.getName());

            listView = (ListView) findViewById(R.id.ls_check_points);
            itemList = SqliteClass.getInstance(context).databasehelp.questionsql.getQuestionDataByEvaluation(ConstValue.getEvaluationId());

            getList();

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Date _date = new Date();
                    String dateFormat = dateFormatDate.format(_date);
                    String hourFormat = dateFormatHour.format(_date);
                    boolean alreadyExists = SqliteClass.getInstance(context).databasehelp.resultsql.checkIfExists(String.valueOf(itemList.get(position).getId()));
                    if(alreadyExists){
                        /* Delete result */
                        SqliteClass.getInstance(context).databasehelp.resultsql.deleteResult(String.valueOf(itemList.get(position).getId()));
                        getList();
                    }
                    else{
                        /*New Instance*/
                        ResultClass resultClass = new ResultClass(
                                0,
                                ConstValue.getUserId(),
                                ConstValue.getEvaluationId(),
                                ConstValue.getEvaluationName(),
                                String.valueOf(itemList.get(position).getId()),
                                String.valueOf(itemList.get(position).getName()),
                                "licencia",
                                otClass.getLastname() + ", " + otClass.getFirstname(),
                                otClass.getLicense(),
                                "SI",
                                dateFormat,
                                hourFormat,
                                "0"
                        );
                        SqliteClass.getInstance(context).databasehelp.resultsql.addResult(resultClass);
                    }
                }
            });
            empty = (TextView) findViewById(R.id.empty);
            if(itemList.size() == 0){ empty.setVisibility(View.VISIBLE); }  else { empty.setVisibility(View.GONE); }
        }
        catch (Exception e){
            Toast.makeText(context, "No hay una evaluación con ese id para mostrar las preguntas respectivas", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    public void getList() {
        itemList = SqliteClass.getInstance(context).databasehelp.questionsql.getQuestionDataByEvaluation(ConstValue.getEvaluationId());
        data = new ArrayList<HashMap<String,String>>();
        for(int z=0; z < itemList.size(); z++){
            QuestionClass cc = itemList.get(z);

            HashMap<String, String> map = new HashMap<String, String>();
            map.put("number", cc.getOrder());
            map.put("description", cc.getName());
            map.put("parameters",cc.getParameters());
            if(SqliteClass.getInstance(context).databasehelp.resultsql.checkIfExists(String.valueOf(itemList.get(z).getId()))){
                map.put("stateDone", "1");
                listView.setItemChecked(z, true);
            }
            else{
                map.put("stateDone", "0");
            }
            data.add(map);
        }
        CheckListAdapter adapter = new CheckListAdapter(context, R.layout.row_check_list, itemList, data);
        listView.setAdapter(adapter);
        listView.setItemsCanFocus(false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_checklist, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == android.R.id.home){
            finish();
            Intent intent = new Intent(CheckListActivity.this, ScanQRActivity.class);
            startActivity(intent);
        }
        else if(id == R.id.action_end_enter){
            boolean allAnswered = SqliteClass.getInstance(context).databasehelp.resultsql.checkAllQuestionsAnswered(ConstValue.getEvaluationId());
            if(allAnswered){
                final Dialog dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.alert_info);

                ImageView image = (ImageView) dialog.findViewById(R.id.alert_info);
                image.setImageResource(R.drawable.ic_alert_info);

                TextView head = (TextView) dialog.findViewById(R.id.alert_info_title);
                head.setText("Sincronización");
                TextView content = (TextView) dialog.findViewById(R.id.alert_info_content);
                content.setText("Desea enviar las preguntas respondidas?");

                Button dialogButtonOk = (Button) dialog.findViewById(R.id.alert_ok);
                dialogButtonOk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new sendResults().execute(true);
                        dialog.dismiss();
                        ConstValue.setResultQR("");
                        startActivity(new Intent(CheckListActivity.this, MainActivity.class));
                    }
                });
                Button dialogButtonCancel = (Button) dialog.findViewById(R.id.alert_cancel);
                dialogButtonCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
            else{
//                Util.info(context, "Sincronización", "");
                final Dialog dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.alert_sync);

                ImageView image = (ImageView) dialog.findViewById(R.id.alert_info);
                image.setImageResource(R.drawable.ic_alert_info);

                TextView head = (TextView) dialog.findViewById(R.id.alert_info_title);
                head.setText("Sincronización");
                TextView content = (TextView) dialog.findViewById(R.id.alert_info_content);
                content.setText("La evaluación no ha sido completada. ¿Desea reportarlo como un rechazó de ingreso?\n");

                Button dialogButtonOk = (Button) dialog.findViewById(R.id.alert_ok);
                dialogButtonOk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new sendResults().execute(true);
                        String url = ConstValue.JSON_SEND_DECLINE + "/" + ConstValue.getResultQR();
                        JSONParser jParser = new JSONParser();
                        JSONObject json = jParser.getJSONFromUrl(url);
                        Log.e("Decline", json.toString());
                        ConstValue.setResultQR("");
                        startActivity(new Intent(CheckListActivity.this, MainActivity.class));
                        dialog.dismiss();
                    }
                });
                Button dialogButtonCancel = (Button) dialog.findViewById(R.id.alert_cancel);
                dialogButtonCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        }
        else if(id == R.id.action_photo){
            Intent intent = new Intent(context, ResultActivity.class);
            context.startActivity(intent);
        }
        else if(id==R.id.action_logout){
            Util.logout(CheckListActivity.this, context);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Util.logout(CheckListActivity.this, context);
    }

    class sendResults extends AsyncTask<Boolean, Void, String> {
        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            dialogOTClosing = ProgressDialog.show(context, "Garita", getString(R.string.action_loading), true);
            super.onPreExecute();
        }
        @Override
        protected void onPostExecute(String result) {
            //startService(new Intent(context, ServicePM.class));
            if (result != null) {
                Toast.makeText(getApplicationContext(), "Error: " + result, Toast.LENGTH_LONG).show();
            } else {
                new imageTask().execute(true);
            }
            // TODO Auto-generated method stub
            dialogOTClosing.dismiss();
        }
        @Override
        protected String doInBackground(Boolean... params) {
            // TODO Auto-generated method stub
            String responseString = null;
            pendingResults = SqliteClass.getInstance(context).databasehelp.resultsql.getPendingResult();
            int total = pendingResults.size();
            try {
                JSONObject jsonobj;
                common = new Common();
                jsonobj = new JSONObject();
                jsonobj.put("total", String.valueOf(total));
                JSONArray results = new JSONArray();
                for (int i=0;i < pendingResults.size(); i++){
                    JSONObject singleResult = new JSONObject();
                    singleResult.put("app","garita");
                    singleResult.put("userCode",pendingResults.get(i).getUserCode());
                    singleResult.put("date",pendingResults.get(i).getDate());
                    singleResult.put("time",pendingResults.get(i).getHour());
                    singleResult.put("evaluationID",pendingResults.get(i).getEvaluationId());
                    singleResult.put("evaluationName",pendingResults.get(i).getEvaluationName());
                    singleResult.put("questionID",pendingResults.get(i).getQuestionId());
                    singleResult.put("personalCode",pendingResults.get(i).getPersonalCode());
                    singleResult.put("personalName",pendingResults.get(i).getPersonalName());
                    singleResult.put("personalDocument",pendingResults.get(i).getPersonalDocument());
                    singleResult.put("response",pendingResults.get(i).getAnswer());
                    singleResult.put("codeEvaluation",ConstValue.getResultQR());
                    results.put(singleResult);
                }
                jsonobj.put("detail",results);
                Log.i("JSON Result", jsonobj.toString());

                List<NameValuePair> nvps = new ArrayList<NameValuePair>();
                nvps.add(new BasicNameValuePair("data", jsonobj.toString()));
                JSONObject json = common.sendJsonData(ConstValue.JSON_SEND_RESULT, nvps);


                if (json.getString("response").equalsIgnoreCase("success")) {
                    //SqliteClass.getInstance(context).databasehelp.otsql.updateOT(otLoad.get(z).getNumberOrder(), "LOAD", "1");
                    Log.i("response", json.getString("response"));
                } else {
                    responseString = json.getString("response");
                }

            } catch (Exception e) {
                responseString = e.toString();
                Log.e("JSON Parser", "Error parsing data " + e.toString());
            }
            return responseString;
        }
        @Override
        protected void onProgressUpdate(Void... values) {
            // TODO Auto-generated method stub
            super.onProgressUpdate(values);
        }
        @Override
        protected void onCancelled() {
            // TODO Auto-generated method stub
            super.onCancelled();
        }
        @Override
        protected void onCancelled(String result) {
            // TODO Auto-generated method stub
            super.onCancelled(result);
        }


    }

    class imageTask extends AsyncTask<Boolean, Void, String> {

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            dataDialog = ProgressDialog.show(context, "Garita", "Procesando. Por favor espere ...", true);
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            dataDialog.dismiss();
            if(result.equals("")){
                final Dialog dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.alert_info);

                ImageView image = (ImageView) dialog.findViewById(R.id.alert_info);
                image.setImageResource(R.drawable.ic_alert_info);

                TextView head = (TextView) dialog.findViewById(R.id.alert_info_title);
                head.setText("Sincronización");
                TextView content = (TextView) dialog.findViewById(R.id.alert_info_content);
                content.setText("Las preguntas e imágenes se enviaron exitosamente.");

                Button dialogButtonOk = (Button) dialog.findViewById(R.id.alert_ok);
                dialogButtonOk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        finish();
                        ConstValue.setResultQR("");
                        startActivity(new Intent(CheckListActivity.this, MainActivity.class));
                    }
                });
                Button dialogButtonCancel = (Button) dialog.findViewById(R.id.alert_cancel);
                dialogButtonCancel.setVisibility(View.GONE);

                dialog.setCanceledOnTouchOutside(false);
                dialog.setCancelable(false);
                dialog.show();
            }
            else{
                Toast.makeText(context, "Error en el envío de imágenes: " + result, Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected String doInBackground(Boolean... params) {
            // TODO Auto-generated method stub
            String responseString = "";

            loadImage = new ArrayList<LoadImageClass>();
            loadImage = SqliteClass.getInstance(context).databasehelp.loadImagesql.getPendingLoadImageData();
            int total = loadImage.size();
            try {
                JSONObject jsonobj;
                common = new Common();
                jsonobj = new JSONObject();
                jsonobj.put("total", String.valueOf(total));

                JSONArray results = new JSONArray();
                for (int i=0;i < loadImage.size(); i++){
                    String file_path = loadImage.get(i).getPhoto();
                    File imageFile = new File(file_path);
                    if(imageFile.exists()) {
                        Bitmap bmInitial = BitmapFactory.decodeFile(file_path);
                        Bitmap bm = Bitmap.createScaledBitmap(bmInitial, 650, 400, false);
                        ByteArrayOutputStream bao = new ByteArrayOutputStream();
                        bm.compress(Bitmap.CompressFormat.JPEG, 90, bao);
                        byte[] ba = bao.toByteArray();
                        String imageEncode = Base64.encodeToString(ba, Base64.DEFAULT);
                        String imageName = ConstValue.getUserId()+"_"+loadImage.get(i).getPhoto()+".jpg";

                        JSONObject singleResult = new JSONObject();
                        singleResult.put("app", "garita");
                        singleResult.put("codeEvaluation",ConstValue.getResultQR());
                        singleResult.put("userCode", loadImage.get(i).getUserCode());
                        singleResult.put("date", loadImage.get(i).getDate());
                        singleResult.put("time", loadImage.get(i).getHour());
                        singleResult.put("evaluationID", loadImage.get(i).getEvaluationId());
                        singleResult.put("evaluationName", loadImage.get(i).getEvaluationName());
                        singleResult.put("personalCode", loadImage.get(i).getPersonalCode());
                        singleResult.put("personalName", loadImage.get(i).getPersonalName());
                        singleResult.put("personalDocument", loadImage.get(i).getPersonalDocument());
                        singleResult.put("namePhoto", loadImage.get(i).getNamePhoto());
                        singleResult.put("photo", imageEncode);
                        singleResult.put("observationPhoto", loadImage.get(i).getObservation());

                        results.put(singleResult);
                    }
                }
                jsonobj.put("detail",results);
                Log.i("JSON Image: ", jsonobj.toString());

                List <NameValuePair> nvps = new ArrayList<NameValuePair>();
                nvps.add(new BasicNameValuePair("data", jsonobj.toString()));
                JSONObject jObj = common.sendJsonData(ConstValue.JSON_SEND_IMAGE, nvps) ;
                Log.i("Image Response: ", jObj.getString("response"));
                if(jObj.getString("response").equalsIgnoreCase("success")){
                    Log.i("response", jObj.getString("response"));
                }
                else{
                    responseString = jObj.getString("response");
                }

            } catch (JSONException e) {
                responseString = e.toString();
                Log.e("JSON Parser", "Error parsing data " + e.toString());
            }
            return responseString;
        }

        @Override
        protected void onCancelled() {
            // TODO Auto-generated method stub
            super.onCancelled();
        }

        @Override
        protected void onCancelled(String result) {
            // TODO Auto-generated method stub
            super.onCancelled(result);
        }

        private ArrayList<HashMap<String, String>> get_items() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            // TODO Auto-generated method stub
            super.onProgressUpdate(values);
        }
    }
}
