package com.gar.views.activities;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.HashMap;

import com.gar.R;
import com.gar.adapters.MenuAdapter;
import com.gar.config.ConstValue;
import com.gar.utils.Util;
import com.gar.views.fragments.HistoryFragment;
import com.gar.views.fragments.HomeFragment;
import com.gar.utils.ConnectionDetector;
import com.gar.views.fragments.ProfileFragment;
import com.gar.views.fragments.SyncFragment;

public class





MainActivity extends ActionBarActivity implements ActionBar.TabListener {
    ActionBar actionBar;
    public static final String TAG = "MainActivity";
    // Whether the Log Fragment is currently shown
    private boolean mLogShown;
    public Context context;
    private DrawerLayout mDrawerLayout;
    private LinearLayout mDeawerView;
    private ActionBarDrawerToggle mDrawerToggle;
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;

    ArrayList<HashMap<String, Integer>> menuArray;
    ListView mListView;
    MenuAdapter mAdapter;

    public SharedPreferences settings;
    public ConnectionDetector cd;

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    private BroadcastReceiver mRegistrationBroadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this;
        settings = getSharedPreferences(ConstValue.MAIN_PREF, 0);
        cd=new ConnectionDetector(this);

        actionBar = getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);

        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle("TMP");

        //getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        LayoutInflater inflator = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflator.inflate(R.layout.action_bar_title, null);
        actionBar.setCustomView(v);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        mTitle = mDrawerTitle = getTitle();
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDeawerView = (LinearLayout)findViewById(R.id.left_drawer);
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close) {
            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                invalidateOptionsMenu();
            }
            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu();
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        menuArray = new ArrayList<HashMap<String,Integer>>();
        HashMap<String, Integer> map = new HashMap<String, Integer>();

        map.put("option", R.string.navigation_garita);
        map.put("image", R.drawable.ic_garita);
        menuArray.add(map);

        /*
        map = new HashMap<String, Integer>();
        map.put("option", R.string.navigation_history);
        map.put("image", R.drawable.ic_history);
        menuArray.add(map);
        */

        map = new HashMap<String, Integer>();
        map.put("option", R.string.navigation_profile);
        map.put("image", R.drawable.ic_profile);
        menuArray.add(map);

        map = new HashMap<String, Integer>();
        map.put("option", R.string.navigation_sync);
        map.put("image", R.drawable.ic_sync_blue);
        menuArray.add(map);

        map = new HashMap<String, Integer>();
        map.put("option", R.string.navigation_logout);
        map.put("image", R.drawable.ic_menu_exit_to_app);
        menuArray.add(map);

        mListView = (ListView)findViewById(R.id.list_navigability);
        mAdapter = new MenuAdapter(getApplicationContext(), menuArray);
        mListView.setAdapter(mAdapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                // TODO Auto-generated method stub
                set_fragment_page(arg2);
            }
        });

        if(getIntent().hasExtra("fragment_position")){
            set_fragment_page(getIntent().getExtras().getInt("fragment_position"));
        }else {
            set_fragment_page(0);
        }
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        int id = item.getItemId();
        if (id == R.id.action_logout) {
            Util.logout(MainActivity.this, context);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onTabSelected(ActionBar.Tab arg0, android.support.v4.app.FragmentTransaction arg1) {
        set_fragment_page(Integer.parseInt(arg0.getTag().toString()));
    }

    @Override
    public void onTabUnselected(ActionBar.Tab arg0, android.support.v4.app.FragmentTransaction arg1) {}

    @Override
    public void onTabReselected(ActionBar.Tab arg0, android.support.v4.app.FragmentTransaction arg1) {}
    public void set_fragment_page(int position){
        Fragment fragment = null;
        Intent intent = null;

        Bundle args;
        switch (position) {
            case 0:
                fragment = new HomeFragment();
                args = new Bundle();
                fragment.setArguments(args);
                break;
                /*
            case 1:
                fragment = new HistoryFragment();
                args = new Bundle();
                fragment.setArguments(args);
                break;
                */
            case 1:
                fragment = new ProfileFragment();
                args = new Bundle();
                fragment.setArguments(args);
                break;
            case 2:
                fragment = new SyncFragment();
                args = new Bundle();
                fragment.setArguments(args);
                break;
            case 3:
                Util.logout(MainActivity.this, context);
                break;


            default:
                break;
        }
        if (intent != null) {
            startActivity(intent);
        }
        if (fragment!=null) {
            FragmentManager fragmentManager = getFragmentManager();
            if (position==0) {
                fragmentManager.beginTransaction()
                        .replace(R.id.sample_content_fragment, fragment)
                        .commit();
            }else{
                fragmentManager.beginTransaction()
                        .replace(R.id.sample_content_fragment, fragment)
                        .addToBackStack(null)
                        .commit();
            }
            mDrawerLayout.closeDrawer(mDeawerView);
        }
    }

    @Override
    public void onBackPressed() {
        Util.logout(MainActivity.this, context);
    }

}
