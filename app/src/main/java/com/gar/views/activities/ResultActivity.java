package com.gar.views.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.gar.R;
import com.gar.adapters.SliderImageLocalAdapter;
import com.gar.config.ConstValue;
import com.gar.db.SqliteClass;
import com.gar.models.LoadImageClass;
import com.gar.utils.Util;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class ResultActivity extends AppCompatActivity {

    Context context;

    ArrayList<HashMap<String, String>> sliderArray;
    TextView name;
    TextView action;
    TextView empty;
    TextView swipeHelp;
    PagerAdapter pageAdapter;
    ViewPager viewPager;
    private ArrayList<LoadImageClass> imageLoad;

    static final int REQUEST_TAKE_PHOTO = 1;
    String mCurrentPhotoPath;
    String imageFileName="";
    Uri photoURI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        context = this;
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Gestor de Fotos"); //TODO poner el nombre de la OT luego

        empty = (TextView) findViewById(android.R.id.empty);
        name = (TextView) findViewById(R.id.name);
        name.setText(ConstValue.getEvaluationName());

        /*
         * Falta el codigo para que funcione el viewPager con los datos traidos de BD y tambien un SliderImageLocalAdapter
         */
        sliderArray = new ArrayList<HashMap<String,String>>();
        imageLoad = SqliteClass.getInstance(context).databasehelp.loadImagesql.getLoadImageData();
        swipeHelp = (TextView) findViewById(R.id.tx_swipe_help);
        if(imageLoad.size() > 1){ swipeHelp.setVisibility(View.VISIBLE);}else {swipeHelp.setVisibility(View.GONE); }

        for(int i = 0; i< imageLoad.size(); i++){
            LoadImageClass cc = imageLoad.get(i);
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("id", String.valueOf(cc.getId()));
            map.put("image", cc.getPhoto());
            map.put("title", cc.getObservation());
            map.put("status", "1");
            sliderArray.add(map);
        }
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        pageAdapter = new SliderImageLocalAdapter(context,sliderArray);
        viewPager.setAdapter(pageAdapter);


        Button btnAction = (Button)findViewById(R.id.photo);
        btnAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dispatchTakePictureIntent();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) {
            System.out.println("SB "+ photoURI);
            ConstValue.setUri(photoURI.toString());
            finish();
            Intent intent = new Intent(ResultActivity.this, PhotoTakenActivity.class);
            startActivity(intent);
        }
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = createImageFile();
                //
            } catch (IOException ex) { }
            if (photoFile != null) {
                photoURI = FileProvider.getUriForFile(this, "com.gar", photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI); //ademas se pone como extra el uri (apuntador) con el nombre y el path
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO); //se hace el intent para tomar foto con el permiso para tomarla
            }
        }
    }

    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName, ".jpg", storageDir);

        mCurrentPhotoPath = image.getAbsolutePath();
        ConstValue.setNamePhoto(image.getName());
        ConstValue.setPath(mCurrentPhotoPath);
        return image;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == android.R.id.home){
            finish();
            Intent intent = new Intent(context, CheckListActivity.class);
            startActivity(intent);
        }
        else if(id==R.id.action_logout){
            Util.logout(ResultActivity.this, context);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Util.logout(ResultActivity.this, context);
    }

}
