package com.gar.views.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.gar.R;
import com.gar.config.ConstValue;
import com.gar.db.SqliteClass;
import com.gar.models.OTClass;
import com.gar.utils.JSONParser;
import com.gar.views.fragments.SyncFragment;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Constructor;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ScanQRActivity extends AppCompatActivity {

    public final int CUSTOMIZED_REQUEST_CODE = 0x0000ffff;
    Context context;
    ProgressDialog dialog;
    IntentResult result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        context = this;
        ConstValue.setResultQR("");
        IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.setCaptureActivity(VerticalActivity.class);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE);
        integrator.setPrompt("Escaneando QR");
        integrator.setOrientationLocked(false);
        integrator.setBeepEnabled(false);
        integrator.initiateScan();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode != CUSTOMIZED_REQUEST_CODE && requestCode != IntentIntegrator.REQUEST_CODE) {
            // This is important, otherwise the result will not be passed to the fragment
            super.onActivityResult(requestCode, resultCode, data);
            return;
        }
        switch (requestCode) {
            case CUSTOMIZED_REQUEST_CODE: {
                Toast.makeText(this, "REQUEST_CODE = " + requestCode, Toast.LENGTH_LONG).show();
                break;
            }
            default:
                break;
        }

        result = IntentIntegrator.parseActivityResult(resultCode, data);
        if(result.getContents() == null) {
            finish();
            ConstValue.setResultQR("");
            Intent intent = new Intent(ScanQRActivity.this, MainActivity.class);
            startActivity(intent);
            Toast.makeText(this, "Escaneo de QR cancelado", Toast.LENGTH_LONG).show();
        }
        else {
            new processingTask().execute(true);
        }
    }

    boolean isValidDate(String inDate) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormat.setLenient(false);
        try {
            dateFormat.parse(inDate.trim());
        } catch (ParseException pe) {
            return false;
        }
        return true;
    }

    class processingTask extends AsyncTask<Boolean, Void, String> {

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            dialog = ProgressDialog.show(ScanQRActivity.this, "Procesando ...", getString(R.string.action_loading), true);
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
                if (isValidDate(result)){

                    ConstValue.setResultQR("");
                    dialog.dismiss();
                    final Dialog dialog = new Dialog(context);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.alert_info);

                    ImageView image = (ImageView) dialog.findViewById(R.id.alert_info);
                    image.setImageResource(R.drawable.ic_alert_info);

                    TextView head = (TextView) dialog.findViewById(R.id.alert_info_title);
                    head.setText("Error");
                    TextView content = (TextView) dialog.findViewById(R.id.alert_info_content);
                    content.setText("Vehículo no puede ingresar por que la cita ya caduco. \n" + result);

                    Button dialogButtonOk = (Button) dialog.findViewById(R.id.alert_ok);
                    final Button dialogButtonCancel = (Button) dialog.findViewById(R.id.alert_cancel);
                    dialogButtonOk.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            finish();
                            Intent intent = new Intent(ScanQRActivity.this, MainActivity.class);
                            startActivity(intent);
                            dialog.dismiss();
                            dialogButtonCancel.setVisibility(View.VISIBLE);
                        }
                    });

                    dialogButtonCancel.setVisibility(View.GONE);
//                    dialogButtonCancel.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            dialog.dismiss();
//                        }
//                    });
                    dialog.show();
//                    Toast.makeText(context, "Vehículo no puede ingresar por que la cita ya caduco." + result, Toast.LENGTH_LONG).show();
                } else {
                    Log.e("Error QR", result);
                    finish();
                    ConstValue.setResultQR("");
                    Intent intent = new Intent(ScanQRActivity.this, MainActivity.class);
                    startActivity(intent);
                    Toast.makeText(context, "QR inválido", Toast.LENGTH_LONG).show();
                    dialog.dismiss();
                }

            } else {
                finish();
                Intent intent = new Intent(ScanQRActivity.this, MainActivity.class);
                startActivity(intent);
                Toast.makeText(context, "Escaneo exitoso: " + ConstValue.getResultQR(), Toast.LENGTH_LONG).show();
                dialog.dismiss();
            }


        }

        @Override
        protected void onProgressUpdate(Void... values) {
            // TODO Auto-generated method stub
            super.onProgressUpdate(values);
        }

        @Override
        protected void onCancelled() {
            // TODO Auto-generated method stub
            super.onCancelled();
        }

        @Override
        protected void onCancelled(String result) {
            // TODO Auto-generated method stub
            super.onCancelled(result);
        }

        @Override
        protected String doInBackground(Boolean... params) {
            String responseString = null;
            ConstValue.setResultQR(result.getContents());
            JSONParser jParser;
            JSONObject json;
            String url = "";
            jParser = new JSONParser();
            url = ConstValue.JSON_OT + ConstValue.getResultQR() + "/";
            Log.i("JSON OT", url);
            try {
                json = jParser.getJSONFromUrl(url);

                if (json.has("data")) {
                    System.out.println("JSON OT: " + json.toString());
                    OTClass otClass = null;
                    JSONObject o = (JSONObject) json.get("data");
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                    Date strDate = dateFormat.parse(o.getString("date"));
                    Date now = new Date();
//                    Date strDate = dateFormat.parse("2021-01-06 13:00:00");
//                    Date now = dateFormat.parse("2021-01-06 13:16:00");
                    long difference = now.getTime() - strDate.getTime();
                    // 900000 equivale a 15 minutos
                    if (true) {
//                    if (difference >= -900000 && difference <= 900000) {
                        otClass = new OTClass(
                                0,
                                o.getString("code"),
                                o.getString("lastName"),
                                o.getString("firstName"),
                                o.getString("license"),
                                o.getString("plate"),
                                o.getString("typeVehicle"),
                                o.getString("typeService"),
                                o.getString("OTSAP"),
                                o.getString("evaluationID"),
                                o.getString("date"));
                        SqliteClass.getInstance(context).databasehelp.otsql.addOT(otClass);
                    } else {
                        responseString = o.getString("date");
                    }
                }
            }
            catch (Exception e) {
                responseString = e.toString();

                /*
                OTClass otClass = null;
                otClass = new OTClass(0, ConstValue.getResultQR(), "A", "H", "555",
                        "555", "none", "none", "t", "1");
                SqliteClass.getInstance(context).databasehelp.otsql.addOT(otClass);
                */
                e.printStackTrace();
            }
            jParser = null; json = null;
            return responseString;
        }
    }
}
