package com.gar.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.gar.R;

import java.util.ArrayList;


public class GeneralSpinnerAdapter extends ArrayAdapter<String> {


    private static LayoutInflater inflater = null;
    private Context context;
    private ArrayList<String> data;
    private String className;


    public GeneralSpinnerAdapter(Context context, int textViewResourceId, ArrayList<String> objects, String className) {
        // TODO Auto-generated constructor stub
        super(context, textViewResourceId, objects);
        this.context = context;
        this.data = objects;
        this.className = className;
        inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub

        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.spinner_row, null);
        }

        TextView label = (TextView) convertView.findViewById(R.id.sp_type);
        label.setText(data.get(position));
        ImageView icon = (ImageView) convertView.findViewById(R.id.icon);


        if(className.equals("SyncFragment")){
            if (data.get(position).equals("SINCRONIZADAS")){
                icon.setImageResource(R.drawable.ic_already_sync);
            }
            else if(data.get(position).equals("SINCRONIZAR")){
                icon.setImageResource(R.drawable.ic_still_sync_white);
            }
        }

        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub

        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.spinner_row, null);
        }


        TextView label = (TextView) convertView.findViewById(R.id.sp_type);
        label.setText(data.get(position));
        label.setTextColor(Color.BLACK);
        ImageView icon = (ImageView) convertView.findViewById(R.id.icon);

        if(className.equals("SyncFragment")){
            if (data.get(position).equals("SINCRONIZADAS")){
                icon.setImageResource(R.drawable.ic_already_sync_black);
            }
            else if(data.get(position).equals("SINCRONIZAR")){
                icon.setImageResource(R.drawable.ic_still_sync_black);
            }
        }


        return convertView;
    }


}