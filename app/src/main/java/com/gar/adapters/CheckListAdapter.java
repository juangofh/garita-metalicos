package com.gar.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.v4.widget.TextViewCompat;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TableRow;
import android.widget.TextView;

import com.gar.R;
import com.gar.models.QuestionClass;

import java.util.ArrayList;
import java.util.HashMap;

public class CheckListAdapter extends ArrayAdapter<QuestionClass> {

    View row;
    ArrayList<QuestionClass> myTeams;
    ArrayList<HashMap<String, String>> data;
    int resLayout;
    Context context;

    public CheckListAdapter(Context context, int textViewResourceId, ArrayList<QuestionClass> myTeams, ArrayList<HashMap<String, String>> data) {
        super(context, textViewResourceId, myTeams);
        this.myTeams = myTeams;
        resLayout = textViewResourceId;
        this.context = context;
        this.data= data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        row = convertView;
        if(row == null) {
            LayoutInflater ll = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = ll.inflate(resLayout, parent, false);
        }

        QuestionClass item = myTeams.get(position); // Produce a row for each Team.
        final HashMap<String, String> map = data.get(position);

        ImageView checked = (ImageView) row.findViewById(R.id.ic_checked);
        if(map.get("stateDone").equals("1")){
            checked.setVisibility(View.VISIBLE);
        }
        else{
            checked.setVisibility(View.GONE);
        }


        TextView description = (TextView) row.findViewById(R.id.tx_description);
        description.setText(map.get("description"));
        TextViewCompat.setAutoSizeTextTypeWithDefaults(description,TextViewCompat.AUTO_SIZE_TEXT_TYPE_UNIFORM);

        TextView number = (TextView) row.findViewById(R.id.tx_number);
        number.setText(map.get("number"));


        ImageView moreInfo = (ImageView) row.findViewById(R.id.infoParameters);
        moreInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(v.getRootView().getContext())
                        .setMessage(map.get("parameters"))
                        .setTitle("Información Adicional")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Log.i("Result","Success");
                            }
                        })
                        .show();
            }
        });

        return row;
    }

}
