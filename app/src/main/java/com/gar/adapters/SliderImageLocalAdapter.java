package com.gar.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.gar.R;
import com.gar.db.SqliteClass;
import com.gar.views.activities.ResultActivity;

import java.util.ArrayList;
import java.util.HashMap;

public class SliderImageLocalAdapter extends PagerAdapter {

    Context context;

    private ArrayList<HashMap<String, String>> postItems;
    public SharedPreferences settings;
	public final String PREFS_NAME = "slider";

    public SliderImageLocalAdapter(Context context, ArrayList<HashMap<String, String>> arraylist){
        this.context = context;
		postItems = arraylist;
        settings = context.getSharedPreferences(PREFS_NAME, 0);
    }

    @Override
    public Object instantiateItem(final ViewGroup container, int position) {
        // TODO Auto-generated method stub
        LayoutInflater inflater = ((Activity)context).getLayoutInflater();
        final HashMap<String, String> map = postItems.get(position);
        View viewItem = inflater.inflate(R.layout.image_item, container, false);
        ImageView imageView = (ImageView) viewItem.findViewById(R.id.imageView);
        imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);

        Glide.with(context)
                .load("file:///"+ map.get("image"))
                .into(imageView);

        ((ViewPager)container).addView(viewItem);

        viewItem.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                final Dialog dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                dialog.setContentView(R.layout.dialog_image_options);

                ImageButton dbOk = (ImageButton) dialog.findViewById(R.id.bt_delete);
                dbOk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        SqliteClass.getInstance(context).databasehelp.loadImagesql.deleteLoadImageById(map.get("id"));
                        Intent intent = new Intent(context, ResultActivity.class);
                        context.startActivity(intent);
                        ((Activity) context).finish();
                        dialog.dismiss();
                    }
                });
                dialog.show();
                return true;
            }
        });
        return viewItem;

    }
 
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
    	return postItems.size();
    }
    public Object getItem(int position) {
		return postItems.get(position);
	}

	public long getItemId(int position) {
		return position;
	}
 
    @Override
    public boolean isViewFromObject(View view, Object object) {
        // TODO Auto-generated method stub
        return view == ((View)object);
    }
 
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // TODO Auto-generated method stub
        ((ViewPager) container).removeView((View) object);
    }
}