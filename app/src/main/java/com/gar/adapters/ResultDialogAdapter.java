package com.gar.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.gar.R;

import java.util.ArrayList;
import java.util.HashMap;

public class ResultDialogAdapter extends BaseAdapter {

    private static LayoutInflater inflater = null;
    private Context context;
    private ArrayList<HashMap<String, String>> data;

    public ResultDialogAdapter(Context context, ArrayList<HashMap<String, String>> data) {
        this.context = context;
        this.data = data;
        inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.row_answer, null);
        }

        TextView question = (TextView) convertView.findViewById(R.id.tx_question);
        TextView answer = (TextView) convertView.findViewById(R.id.tx_answer);
        ImageView icon = (ImageView) convertView.findViewById(R.id.ic_result);

        final HashMap<String, String> map = data.get(position);
        question.setText(map.get("question"));
        if (!map.get("answer").equals("")) {
            icon.setImageResource(R.drawable.ic_pencil);
            answer.setText(map.get("answer"));
        } else {
            icon.setImageResource(R.drawable.ic_alert_red);
            answer.setText("No tiene respuesta.");
        }

        return convertView;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

}