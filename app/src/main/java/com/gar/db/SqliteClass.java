package com.gar.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.File;
import java.util.ArrayList;

import com.gar.models.*;

public class SqliteClass {
    public DatabaseHelper databasehelp;
    private static SqliteClass SqliteInstance = null;

    private SqliteClass(Context context) {
        databasehelp = new DatabaseHelper(context);
    }

    public static SqliteClass getInstance(Context context) {
        if (SqliteInstance == null) {
            SqliteInstance = new SqliteClass(context);
        }
        return SqliteInstance;
    }

    public class DatabaseHelper extends SQLiteOpenHelper {

        public Context context;
        private static final int DATABASE_VERSION = 1;
        private static final String DATABASE_NAME = "app_garita.db";

        /* @TABLE_APP_USER */
        public static final String TABLE_APP_USER = "app_user";
        public static final String KEY_USEID = "au_user_id";
        public static final String KEY_USEUSE = "au_user_use";
        public static final String KEY_USEPAS = "au_user_pas";
        public static final String KEY_USENAM = "au_user_nam";
        public static final String KEY_USEEMA = "au_user_ema";
        public static final String KEY_USECOM = "au_user_com";
        public static final String KEY_USEACT = "au_user_act";

        /* @TABLE_APP_OT */
        public static final String TABLE_APP_OT = "app_ot";
        private static final String KEY_OTID = "ao_id";
        private static final String KEY_OTCOD = "ao_cod";
        private static final String KEY_OTLASNAM = "ao_las_nam";
        private static final String KEY_OTFIRNAM = "ao_fir_nam";
        private static final String KEY_OTLIC = "ao_lic";
        private static final String KEY_OTPLA = "ao_pla";
        private static final String KEY_OTTYPVEH = "ao_typ_veh";
        private static final String KEY_OTTYPSER = "ao_typ_ser";
        private static final String KEY_OTSAP = "ao_sap";
        private static final String KEY_OTEVAID = "ao_eva_id";
        private static final String KEY_OTDATE = "ao_date";

        /* @TABLE_APP_EVALUATION */
        public static final String TABLE_APP_EVALUATION = "app_evaluation";
        private static final String KEY_EVAID = "ae_id";
        private static final String KEY_EVANAM = "ae_nam";
        private static final String KEY_EVASTADAT = "ae_sta_dat";
        private static final String KEY_EVAENDDAT = "ae_end_dat";
        private static final String KEY_EVAAPP = "ae_app";
        private static final String KEY_EVALIN = "ae_lin";
        private static final String KEY_EVATYP = "ae_typ";
        private static final String KEY_EVAACT = "ae_act";

        /* @TABLE_APP_QUESTION */
        public static final String TABLE_APP_QUESTION = "app_question";
        private static final String KEY_QUEID = "aq_id";
        private static final String KEY_QUENAM = "aq_nam";
        private static final String KEY_QUEEVAID = "aq_eva_id";
        private static final String KEY_QUEORD = "aq_ord";
        private static final String KEY_QUETYP = "aq_typ";
        private static final String KEY_QUEPAR = "aq_par";
        private static final String KEY_QUEALT = "aq_alt";
        private static final String KEY_QUEACT = "aq_act";



        /* @TABLE_APP_RESULT */
        private static final String TABLE_APP_RESULT = "app_result";
        private static final String KEY_RESID = "ar_id";
        private static final String KEY_RESUSECOD = "ar_use_cod";
        private static final String KEY_RESEVAID = "ar_eva_id";
        private static final String KEY_RESEVANAM = "ar_eva_nam";
        private static final String KEY_RESQUEID = "ar_que_id";
        private static final String KEY_RESQUENAM = "ar_que_nam";
        private static final String KEY_RESPERCOD = "ar_per_cod";
        private static final String KEY_RESPERNAM = "ar_per_nam";
        private static final String KEY_RESPERDOC = "ar_per_doc";
        private static final String KEY_RESANS = "ar_ans";
        private static final String KEY_RESDAT = "ar_dat";
        private static final String KEY_RESHOU = "ar_hou";
        private static final String KEY_RESLOA = "ar_loa";

        /* @TABLE_APP_LOAD_IMAGE */
        private static final String TABLE_APP_LOAD_IMAGE = "app_load_image";
        private static final String KEY_LOAIMAID = "ai_load_id";
        private static final String KEY_LOAIMAUSECOD = "ai_load_use_cod";
        private static final String KEY_LOAIMAEVAID = "ai_load_eva_id";
        private static final String KEY_LOAIMAEVANAM = "ai_load_eva_nam";
        private static final String KEY_LOAIMAPERCOD = "ai_load_per_cod";
        private static final String KEY_LOAIMAPERNAM = "ai_load_per_nam";
        private static final String KEY_LOAIMAPERDOC = "ai_load_per_doc";
        private static final String KEY_LOAIMADAT = "ai_load_dat";
        private static final String KEY_LOAIMAHOU = "ai_load_hou";
        private static final String KEY_LOAIMANAMPHO = "ai_load_nam_pho";
        private static final String KEY_LOAIMAPHO = "ai_load_pho";
        private static final String KEY_LOAIMAOBS = "ai_load_obs";
        private static final String KEY_LOAIMALOA = "ai_load_loa";

        /* @TABLE_APP_SYNCHRONIZATION */
        public static final String TABLE_APP_SYNCHRONIZATION = "app_synchronization";
        public static final String KEY_SYNID = "as_id";
        public static final String KEY_SYNCLANAM = "as_cla_nam";
        public static final String KEY_SYNDATHOUSTA = "as_dat_hou_sta";
        public static final String KEY_SYNDATHOUEND = "as_dat_hou_end";
        public static final String KEY_SYNREGNUM = "as_reg_num";
        public static final String KEY_SYNFAINUM = "as_fai_num";
        public static final String KEY_SYNSTA = "as_sta";


        /* @SQL */
        public AppUserSql usersql;
        public AppOTSql otsql;
        public AppEvaluationSql evaluationsql;
        public AppQuestionSql questionsql;
        public AppResultSql resultsql;
        public AppLoadImageSql loadImagesql;
        public AppSynchronizationSql synchronizationsql;


        public DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
            this.context = context;
            usersql = new AppUserSql();
            otsql = new AppOTSql();
            evaluationsql = new AppEvaluationSql();
            questionsql = new AppQuestionSql();
            resultsql = new AppResultSql();
            loadImagesql = new AppLoadImageSql();
            synchronizationsql = new AppSynchronizationSql();
        }
        public void onCreate(SQLiteDatabase db) {
            /* @TABLE_USER */
            String CREATE_TABLE_USER = "CREATE TABLE " + TABLE_APP_USER + "("
                    + KEY_USEID + " INTEGER PRIMARY KEY," + KEY_USEUSE + " TEXT,"
                    + KEY_USEPAS + " TEXT,"  + KEY_USENAM + " TEXT," + KEY_USEEMA + " TEXT," + KEY_USECOM+ " TEXT," + KEY_USEACT + " TEXT )";

            /* @TABLE_OT */
            String CREATE_TABLE_OT = "CREATE TABLE " + TABLE_APP_OT + "(" + KEY_OTID
                    + " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_OTCOD + " TEXT," + KEY_OTLASNAM + " TEXT," + KEY_OTFIRNAM + " TEXT," + KEY_OTLIC
                    + " TEXT," + KEY_OTPLA + " TEXT,"+ KEY_OTTYPVEH + " TEXT," + KEY_OTTYPSER + " TEXT," + KEY_OTSAP + " TEXT," + KEY_OTEVAID + " TEXT,"
                    + KEY_OTDATE +" TEXT )";

            /* @TABLE_EVALUATION */
            String CREATE_TABLE_EVALUATION = "CREATE TABLE " + TABLE_APP_EVALUATION + "(" + KEY_EVAID
                    + " INTEGER PRIMARY KEY," + KEY_EVANAM + " TEXT," + KEY_EVASTADAT + " TEXT," + KEY_EVAENDDAT + " TEXT," + KEY_EVAAPP
                    + " TEXT," + KEY_EVALIN + " TEXT,"+ KEY_EVATYP + " TEXT," + KEY_EVAACT  + " TEXT)";

            /* @TABLE_QUESTION */
            String CREATE_TABLE_QUESTION = "CREATE TABLE " + TABLE_APP_QUESTION + "(" + KEY_QUEID
                    + " INTEGER PRIMARY KEY," + KEY_QUENAM + " TEXT," + KEY_QUEEVAID + " TEXT," + KEY_QUEORD
                    + " TEXT," + KEY_QUETYP + " TEXT," + KEY_QUEPAR + " TEXT," + KEY_QUEALT + " TEXT," + KEY_QUEACT + " TEXT)";


            /* @TABLE_RESULT */
            String CREATE_TABLE_RESULT = "CREATE TABLE " + TABLE_APP_RESULT + "(" + KEY_RESID
                    + " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_RESUSECOD + " TEXT," + KEY_RESEVAID + " TEXT," + KEY_RESEVANAM
                    + " TEXT," + KEY_RESQUEID + " TEXT," + KEY_RESQUENAM + " TEXT," + KEY_RESPERCOD
                    + " TEXT,"  + KEY_RESPERNAM + " TEXT," + KEY_RESPERDOC + " TEXT," +KEY_RESANS + " TEXT," + KEY_RESDAT
                    + " TEXT," + KEY_RESHOU + " TEXT," + KEY_RESLOA  + " TEXT)";


            /* @TABLE_LOAD_IMAGE */
            String CREATE_TABLE_LOAD_IMAGE = "CREATE TABLE " + TABLE_APP_LOAD_IMAGE + "(" + KEY_LOAIMAID
                    + " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_LOAIMAUSECOD + " TEXT," + KEY_LOAIMAEVAID + " TEXT," + KEY_LOAIMAEVANAM
                    + " TEXT," + KEY_LOAIMAPERCOD + " TEXT," + KEY_LOAIMAPERNAM + " TEXT," + KEY_LOAIMAPERDOC
                    + " TEXT,"  + KEY_LOAIMADAT + " TEXT," + KEY_LOAIMAHOU + " TEXT," +KEY_LOAIMANAMPHO + " TEXT,"+ KEY_LOAIMAPHO
                    + " TEXT," + KEY_LOAIMAOBS + " TEXT," + KEY_LOAIMALOA + " TEXT)";

            /* @TABLE_SYNCHRONIZATION */
            String CREATE_TABLE_SYNCHRONIZATION = "CREATE TABLE " + TABLE_APP_SYNCHRONIZATION + "("
                    + KEY_SYNID + " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_SYNCLANAM + " TEXT,"
                    + KEY_SYNDATHOUSTA + " TEXT,"  + KEY_SYNDATHOUEND + " TEXT," + KEY_SYNREGNUM + " TEXT,"
                    + KEY_SYNFAINUM+ " TEXT," + KEY_SYNSTA + " TEXT )";


            /* @EXECSQL_CREATE */
            db.execSQL(CREATE_TABLE_USER);
            db.execSQL(CREATE_TABLE_OT);
            db.execSQL(CREATE_TABLE_EVALUATION);
            db.execSQL(CREATE_TABLE_QUESTION);
            db.execSQL(CREATE_TABLE_RESULT);
            db.execSQL(CREATE_TABLE_LOAD_IMAGE);
            db.execSQL(CREATE_TABLE_SYNCHRONIZATION);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            /* @EXECSQL_DROP */
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_APP_USER);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_APP_OT);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_APP_EVALUATION);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_APP_QUESTION);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_APP_RESULT);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_APP_LOAD_IMAGE);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_APP_SYNCHRONIZATION);
            onCreate(db);
        }
        public boolean checkDataBase(){
            File dbFile = new File(context.getDatabasePath(DATABASE_NAME).toString());
            return dbFile.exists();
        }
        public void deleteDataBase(){
            context.deleteDatabase(DATABASE_NAME);
        }


        /* @CLASS_USERSQL */
        public class AppUserSql {
            public AppUserSql() {	}
            public void deleteUser(){
                SQLiteDatabase db = databasehelp.getWritableDatabase();
                db.delete(TABLE_APP_USER,null,null);
                db.close();
            }
            public void addUser(UserClass user) {
                SQLiteDatabase db = databasehelp.getWritableDatabase();
                ContentValues values = new ContentValues();
                values.put(KEY_USEID, user.getId());
                values.put(KEY_USEUSE, user.getUsername());
                values.put(KEY_USEPAS, user.getPassword());
                values.put(KEY_USENAM, user.getName());
                values.put(KEY_USEEMA, user.getEmail());
                values.put(KEY_USECOM, user.getCompany());
                values.put(KEY_USEACT, user.getActive());

                db.insert(TABLE_APP_USER, null, values);
                db.close();
            }

            public boolean isRegisterUser(String sName, String sPassword) {
                SQLiteDatabase db = databasehelp.getReadableDatabase();
                Cursor cursor = db.query(TABLE_APP_USER, new String[] { KEY_USEID, KEY_USEUSE, KEY_USEPAS, KEY_USENAM, KEY_USEEMA,
                                KEY_USECOM, KEY_USEACT}, KEY_USEUSE + "=?" + " and "+ KEY_USEPAS + "=?",
                        new String[] {sName, sPassword}, null, null, null, null);
                if (cursor != null){
                    cursor.moveToFirst();
                }
                if(cursor.getCount() == 1){
                    db.close();
                    return true;
                }else{
                    db.close();
                    return false;
                }
            }
            public UserClass getUser(int id) {
                SQLiteDatabase db = databasehelp.getReadableDatabase();
                Cursor cursor = db.query(TABLE_APP_USER, new String[] { KEY_USEID, KEY_USEUSE, KEY_USEPAS, KEY_USENAM, KEY_USEEMA,
                                KEY_USECOM, KEY_USEACT}, KEY_USEID + "=?",
                        new String[] { String.valueOf(id) }, null, null, null, null);
                if (cursor != null){
                    cursor.moveToFirst();
                }
                UserClass user = new UserClass(cursor.getInt(0), cursor.getString(1),
                        cursor.getString(2), cursor.getString(3), cursor.getString(4),
                        cursor.getString(5), cursor.getString(5));
                db.close();
                return user;
            }
            public String getData(int nField, String sName) {
                String _date = null;
                SQLiteDatabase db = databasehelp.getReadableDatabase();
                Cursor cursor = db.query(TABLE_APP_USER, new String[] { KEY_USEID, KEY_USEUSE, KEY_USEPAS, KEY_USENAM, KEY_USEEMA,
                                KEY_USECOM, KEY_USEACT}, KEY_USEUSE + "=?",
                        new String[] {sName}, null, null, null, null);
                if (cursor != null){
                    cursor.moveToFirst();
                }
                _date = cursor.getString(nField);
                db.close();
                return _date;
            }
            public ArrayList<UserClass> getUserData() {
                ArrayList<UserClass> userList = new ArrayList<UserClass>();
                String selectQuery = "SELECT * FROM " + TABLE_APP_USER + " LIMIT 1";
                SQLiteDatabase db = databasehelp.getWritableDatabase();
                Cursor cursor = db.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    do {
                        UserClass user = new UserClass();
                        user.setId(cursor.getInt(cursor.getColumnIndex(KEY_USEID)));
                        user.setUsername(cursor.getString(cursor.getColumnIndex(KEY_USEUSE)));
                        user.setPassword(cursor.getString(cursor.getColumnIndex(KEY_USEPAS)));
                        user.setName(cursor.getString(cursor.getColumnIndex(KEY_USENAM)));
                        user.setEmail(cursor.getString(cursor.getColumnIndex(KEY_USEEMA)));
                        user.setCompany(cursor.getString(cursor.getColumnIndex(KEY_USECOM)));
                        user.setActive(cursor.getString(cursor.getColumnIndex(KEY_USEACT)));
                        userList.add(user);
                    } while (cursor.moveToNext());
                }
                db.close();
                return userList;
            }

            public int getId(String sName, String sPassword) {
                int _id=0;
                SQLiteDatabase db = databasehelp.getReadableDatabase();
                Cursor cursor = db.query(TABLE_APP_USER, new String[] { KEY_USEID, KEY_USEUSE,
                                KEY_USEPAS, KEY_USENAM, KEY_USEEMA, KEY_USECOM, KEY_USEACT}, KEY_USEUSE + "=?" + " and "+ KEY_USEPAS + "=?",
                        new String[] {sName, sPassword}, null, null, null, null);
                if (cursor != null){
                    cursor.moveToFirst();
                }
                _id = cursor.getInt(0);
                db.close();
                return _id;
            }
        }

        /* @CLASS_OTSQL */
        public class AppOTSql{
            public AppOTSql() {}
            public void deleteOT(){
                SQLiteDatabase db = databasehelp.getWritableDatabase();
                db.delete(TABLE_APP_OT,null,null);
                db.close();
            }
            public void addOT(OTClass ot) {
                SQLiteDatabase db = databasehelp.getWritableDatabase();
                ContentValues values = new ContentValues();
                //values.put(KEY_OTID, ot.getId());
                values.put(KEY_OTCOD, ot.getOtCode());
                values.put(KEY_OTLASNAM, ot.getLastname());
                values.put(KEY_OTFIRNAM, ot.getFirstname());
                values.put(KEY_OTLIC, ot.getLicense());
                values.put(KEY_OTPLA, ot.getPlate());
                values.put(KEY_OTTYPVEH, ot.getTypeVehicle());
                values.put(KEY_OTTYPSER, ot.getTypeService());
                values.put(KEY_OTSAP, ot.getOtSap());
                values.put(KEY_OTEVAID, ot.getEvaluationID());
                values.put(KEY_OTDATE, ot.getDate());
                db.insert(TABLE_APP_OT, null, values);
                db.close();
            }

            public ArrayList<OTClass> getOTData() {
                ArrayList<OTClass> otList = new ArrayList<OTClass>();
                String selectQuery = "SELECT * FROM " + TABLE_APP_OT;
                SQLiteDatabase db = databasehelp.getWritableDatabase();
                Cursor cursor = db.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    do {
                        OTClass ot = new OTClass();
                        ot.setId(cursor.getInt(cursor.getColumnIndex(KEY_OTID)));
                        ot.setOtCode(cursor.getString(cursor.getColumnIndex(KEY_OTCOD)));
                        ot.setLastname(cursor.getString(cursor.getColumnIndex(KEY_OTLASNAM)));
                        ot.setFirstname(cursor.getString(cursor.getColumnIndex(KEY_OTFIRNAM)));
                        ot.setLicense(cursor.getString(cursor.getColumnIndex(KEY_OTLIC)));
                        ot.setPlate(cursor.getString(cursor.getColumnIndex(KEY_OTPLA)));
                        ot.setTypeVehicle(cursor.getString(cursor.getColumnIndex(KEY_OTTYPVEH)));
                        ot.setTypeService(cursor.getString(cursor.getColumnIndex(KEY_OTTYPSER)));
                        ot.setOtSap(cursor.getString(cursor.getColumnIndex(KEY_OTSAP)));
                        ot.setEvaluationID(cursor.getString(cursor.getColumnIndex(KEY_OTEVAID)));

                        otList.add(ot);
                    } while (cursor.moveToNext());
                }
                db.close();
                return otList;
            }


            public OTClass getOT(String otSap) {
                SQLiteDatabase db = databasehelp.getReadableDatabase();
                Cursor cursor = db.query(TABLE_APP_OT, new String[] {KEY_OTID, KEY_OTCOD, KEY_OTLASNAM,
                                KEY_OTFIRNAM, KEY_OTLIC, KEY_OTPLA, KEY_OTTYPVEH, KEY_OTTYPSER, KEY_OTSAP,
                                KEY_OTEVAID,KEY_OTDATE}, KEY_OTCOD + "=?",
                        new String[] { otSap }, null, null, null, null);
                if (cursor != null){
                    cursor.moveToFirst();
                }
                OTClass otClass = new OTClass(cursor.getInt(0), cursor.getString(1), cursor.getString(2),
                        cursor.getString(3), cursor.getString(4), cursor.getString(5),
                        cursor.getString(6), cursor.getString(7), cursor.getString(8),
                        cursor.getString(9), cursor.getString(10));
                db.close();
                return otClass;
            }

            public String getData(int nField, String otSap) {
                String _data = null;
                SQLiteDatabase db = databasehelp.getReadableDatabase();
                Cursor cursor = db.query(TABLE_APP_QUESTION, new String[] {KEY_OTID, KEY_OTCOD, KEY_OTLASNAM,
                                KEY_OTFIRNAM, KEY_OTLIC, KEY_OTPLA, KEY_OTTYPVEH, KEY_OTTYPSER, KEY_OTSAP, KEY_OTEVAID}, KEY_OTCOD + "=?",
                        new String[] {otSap}, null, null, null, null);
                if (cursor != null){
                    cursor.moveToFirst();
                }
                _data = cursor.getString(nField);
                db.close();
                return _data;
            }
        }

        public class AppEvaluationSql{
            public AppEvaluationSql() {}

            public void deleteEvaluation(){
                SQLiteDatabase db = databasehelp.getWritableDatabase();
                db.delete(TABLE_APP_EVALUATION,null,null);
                db.close();
            }
            public boolean checkIfExists(String id){
                int result = 0;
                SQLiteDatabase db = databasehelp.getWritableDatabase();
                Cursor mCount= db.rawQuery("SELECT COUNT(*) FROM " + TABLE_APP_EVALUATION+ " WHERE " + KEY_EVAID +"='" + id + "'",null);
                mCount.moveToFirst();
                result= mCount.getInt(0);
                mCount.close();
                db.close();
                return result > 0;
            }
            public void addEvaluation(EvaluationClass evaluationClass) {
                SQLiteDatabase db = databasehelp.getWritableDatabase();
                ContentValues values = new ContentValues();
                values.put(KEY_EVAID, evaluationClass.getId());
                values.put(KEY_EVANAM, evaluationClass.getName());
                values.put(KEY_EVASTADAT, evaluationClass.getStartDate());
                values.put(KEY_EVAENDDAT, evaluationClass.getEndDate());
                values.put(KEY_EVAAPP, evaluationClass.getApp());
                values.put(KEY_EVALIN, evaluationClass.getLink());
                values.put(KEY_EVATYP, evaluationClass.getType());
                values.put(KEY_EVAACT, evaluationClass.getActive());
                db.insert(TABLE_APP_EVALUATION, null, values);
                db.close();
            }
            public void updateEvaluation(EvaluationClass evaluationClass, String idComing) {
                SQLiteDatabase db = databasehelp.getWritableDatabase();
                ContentValues values = new ContentValues();
                values.put(KEY_EVAID, evaluationClass.getId());
                values.put(KEY_EVANAM, evaluationClass.getName());
                values.put(KEY_EVASTADAT, evaluationClass.getStartDate());
                values.put(KEY_EVAENDDAT, evaluationClass.getEndDate());
                values.put(KEY_EVAAPP, evaluationClass.getApp());
                values.put(KEY_EVALIN, evaluationClass.getLink());
                values.put(KEY_EVATYP, evaluationClass.getType());
                values.put(KEY_EVAACT, evaluationClass.getActive());
                db.update(TABLE_APP_EVALUATION, values, KEY_EVAID + " = ?",new String[] { idComing});
                db.close();
            }

            public ArrayList<EvaluationClass> getEvaluationData() {
                ArrayList<EvaluationClass> otList = new ArrayList<EvaluationClass>();
                String selectQuery = "SELECT * FROM " + TABLE_APP_EVALUATION;
                SQLiteDatabase db = databasehelp.getWritableDatabase();
                Cursor cursor = db.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    do {
                        EvaluationClass evaluationClass = new EvaluationClass();
                        evaluationClass.setId(cursor.getInt(cursor.getColumnIndex(KEY_EVAID)));
                        evaluationClass.setName(cursor.getString(cursor.getColumnIndex(KEY_EVANAM)));
                        evaluationClass.setStartDate(cursor.getString(cursor.getColumnIndex(KEY_EVASTADAT)));
                        evaluationClass.setEndDate(cursor.getString(cursor.getColumnIndex(KEY_EVAENDDAT)));
                        evaluationClass.setApp(cursor.getString(cursor.getColumnIndex(KEY_EVAAPP)));
                        evaluationClass.setLink(cursor.getString(cursor.getColumnIndex(KEY_EVALIN)));
                        evaluationClass.setType(cursor.getString(cursor.getColumnIndex(KEY_EVATYP)));
                        evaluationClass.setActive(cursor.getString(cursor.getColumnIndex(KEY_EVAACT)));

                        otList.add(evaluationClass);
                    } while (cursor.moveToNext());
                }
                db.close();
                return otList;
            }


            public EvaluationClass getEvaluation(String id) {
                SQLiteDatabase db = databasehelp.getReadableDatabase();
                Cursor cursor = db.query(TABLE_APP_EVALUATION, new String[]{KEY_EVAID, KEY_EVANAM, KEY_EVASTADAT,
                                KEY_EVAENDDAT, KEY_EVAAPP, KEY_EVALIN, KEY_EVATYP, KEY_EVAACT}, KEY_EVAID + "=?",
                        new String[]{id}, null, null, null, null);
                if (cursor != null) {
                    cursor.moveToFirst();
                }

                System.out.println("Vas a caer Lucho " + cursor ==null);

                EvaluationClass otClass = new EvaluationClass(cursor.getInt(0), cursor.getString(1), cursor.getString(2),
                        cursor.getString(3), cursor.getString(4), cursor.getString(5),
                        cursor.getString(6),"","", cursor.getString(7));
                cursor.close();
                db.close();
                return otClass;
            }

            public String getData(int nField, String id) {
                String _data = null;
                SQLiteDatabase db = databasehelp.getReadableDatabase();
                Cursor cursor = db.query(TABLE_APP_QUESTION, new String[] {KEY_EVAID, KEY_EVANAM, KEY_EVASTADAT,
                                KEY_EVAENDDAT, KEY_EVAAPP, KEY_EVALIN, KEY_EVATYP, KEY_EVAACT}, KEY_EVAID + "=?",
                        new String[] {id}, null, null, null, null);
                if (cursor != null){
                    cursor.moveToFirst();
                }
                _data = cursor.getString(nField);
                db.close();
                return _data;
            }


        }

        /* @CLASS_QUESTION_SQL */
        public class AppQuestionSql {
            public AppQuestionSql() {}
            public void deleteQuestion(){
                SQLiteDatabase db = databasehelp.getWritableDatabase();
                db.delete(TABLE_APP_QUESTION,null,null);
                db.close();
            }
            public boolean checkIfExists(String id){
                int result = 0;
                SQLiteDatabase db = databasehelp.getWritableDatabase();
                Cursor mCount= db.rawQuery("SELECT COUNT(*) FROM " + TABLE_APP_QUESTION+ " WHERE " + KEY_QUEID +"='" + id + "'",null);
                mCount.moveToFirst();
                result= mCount.getInt(0);
                mCount.close();
                db.close();
                return result > 0;
            }
            public void addQuestion(QuestionClass question) {
                SQLiteDatabase db = databasehelp.getWritableDatabase();
                ContentValues values = new ContentValues();
                values.put(KEY_QUEID, question.getId());
                values.put(KEY_QUENAM, question.getName());
                values.put(KEY_QUEEVAID, question.getEvaluationID());
                values.put(KEY_QUEORD, question.getOrder());
                values.put(KEY_QUETYP, question.getType());
                values.put(KEY_QUEPAR, question.getParameters());
                values.put(KEY_QUEALT, question.getAlternatives());
                values.put(KEY_QUEACT, question.getActive());
                db.insert(TABLE_APP_QUESTION, null, values);
                db.close();
            }
            public void updateQuestion(QuestionClass question, String idComing) {
                SQLiteDatabase db = databasehelp.getWritableDatabase();
                ContentValues values = new ContentValues();
                values.put(KEY_QUEID, question.getId());
                values.put(KEY_QUENAM, question.getName());
                values.put(KEY_QUEEVAID, question.getEvaluationID());
                values.put(KEY_QUEORD, question.getOrder());
                values.put(KEY_QUETYP, question.getType());
                values.put(KEY_QUEPAR, question.getParameters());
                values.put(KEY_QUEALT, question.getAlternatives());
                values.put(KEY_QUEACT, question.getActive());
                db.update(TABLE_APP_QUESTION, values, KEY_QUEID + " = ?",new String[] { idComing});
                db.close();
            }


            public ArrayList<QuestionClass> getQuestionData() {
                ArrayList<QuestionClass> questionList = new ArrayList<QuestionClass>();
                String selectQuery = "SELECT * FROM " + TABLE_APP_QUESTION;
                SQLiteDatabase db = databasehelp.getWritableDatabase();
                Cursor cursor = db.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    do {
                        QuestionClass hive = new QuestionClass();
                        hive.setId(cursor.getInt(cursor.getColumnIndex(KEY_QUEID)));
                        hive.setName(cursor.getString(cursor.getColumnIndex(KEY_QUENAM)));
                        hive.setEvaluationID(cursor.getString(cursor.getColumnIndex(KEY_QUEEVAID)));
                        hive.setOrder(cursor.getString(cursor.getColumnIndex(KEY_QUEORD)));
                        hive.setType(cursor.getString(cursor.getColumnIndex(KEY_QUETYP)));
                        hive.setParameters(cursor.getString(cursor.getColumnIndex(KEY_QUEPAR)));
                        hive.setAlternatives(cursor.getString(cursor.getColumnIndex(KEY_QUEALT)));
                        hive.setActive(cursor.getString(cursor.getColumnIndex(KEY_QUEACT)));
                        questionList.add(hive);
                    } while (cursor.moveToNext());
                }
                db.close();
                return questionList;
            }

            public ArrayList<QuestionClass> getQuestionDataByEvaluation(String id) {
                ArrayList<QuestionClass> questionList = new ArrayList<QuestionClass>();
                String selectQuery = "SELECT * FROM " + TABLE_APP_QUESTION + " WHERE " + KEY_QUEEVAID + " ='" + id + "'";
                SQLiteDatabase db = databasehelp.getWritableDatabase();
                Cursor cursor = db.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    do {
                        QuestionClass hive = new QuestionClass();
                        hive.setId(cursor.getInt(cursor.getColumnIndex(KEY_QUEID)));
                        hive.setName(cursor.getString(cursor.getColumnIndex(KEY_QUENAM)));
                        hive.setEvaluationID(cursor.getString(cursor.getColumnIndex(KEY_QUEEVAID)));
                        hive.setOrder(cursor.getString(cursor.getColumnIndex(KEY_QUEORD)));
                        hive.setType(cursor.getString(cursor.getColumnIndex(KEY_QUETYP)));
                        hive.setParameters(cursor.getString(cursor.getColumnIndex(KEY_QUEPAR)));
                        hive.setAlternatives(cursor.getString(cursor.getColumnIndex(KEY_QUEALT)));
                        hive.setActive(cursor.getString(cursor.getColumnIndex(KEY_QUEACT)));
                        questionList.add(hive);
                    } while (cursor.moveToNext());
                }
                cursor.close();
                db.close();
                return questionList;
            }


            public QuestionClass getQuestion(int nId) {
                SQLiteDatabase db = databasehelp.getReadableDatabase();
                Cursor cursor = db.query(TABLE_APP_QUESTION, new String[] {KEY_QUEID, KEY_QUENAM,
                                KEY_QUEEVAID, KEY_QUEORD, KEY_QUETYP, KEY_QUEPAR, KEY_QUEALT, KEY_QUEACT}, KEY_QUEID + "=?",
                        new String[] { String.valueOf(nId) }, null, null, null, null);
                if (cursor != null){
                    cursor.moveToFirst();
                }
                QuestionClass hive = new QuestionClass(cursor.getInt(0), cursor.getString(1), cursor.getString(2),
                        cursor.getString(3), cursor.getString(4),cursor.getString(5),cursor.getString(6),cursor.getString(7));
                db.close();
                return hive;
            }

            public String getData(int nField, String sName) {
                String _data = null;
                SQLiteDatabase db = databasehelp.getReadableDatabase();
                Cursor cursor = db.query(TABLE_APP_QUESTION, new String[] {KEY_QUEID, KEY_QUENAM,
                                KEY_QUEEVAID, KEY_QUEORD, KEY_QUETYP, KEY_QUEPAR, KEY_QUEALT, KEY_QUEACT}, KEY_QUEID + "=?",
                        new String[] {sName}, null, null, null, null);
                if (cursor != null){
                    cursor.moveToFirst();
                }
                _data = cursor.getString(nField);
                db.close();
                return _data;
            }
        }

        public class AppResultSql{
            public AppResultSql() {}
            public void deleteResult(){
                SQLiteDatabase db = databasehelp.getWritableDatabase();
                db.delete(TABLE_APP_RESULT,null,null);
                db.close();
            }

            public void deleteResult(String questionId){
                SQLiteDatabase db = databasehelp.getWritableDatabase();
                db.delete(TABLE_APP_RESULT,KEY_RESQUEID + " = ?",new String[] { questionId});
                db.close();
            }

            public boolean checkIfExists(String questionId){
                int result = 0;
                SQLiteDatabase db = databasehelp.getWritableDatabase();
                Cursor mCount= db.rawQuery("SELECT COUNT(*) FROM " + TABLE_APP_RESULT + " WHERE " + KEY_RESQUEID + "='" + questionId + "'",null);
                mCount.moveToFirst();
                result= mCount.getInt(0);
                mCount.close();
                db.close();
                return result > 0;
            }

            public boolean checkAllQuestionsAnswered(String evaluationId){
                ArrayList<QuestionClass> questions = databasehelp.questionsql.getQuestionDataByEvaluation(evaluationId);
                for(int i = 0; i < questions.size(); i++){
                    if(!checkIfExists(String.valueOf(questions.get(i).getId()))){
                        return false;
                    }
                }
                return true;
            }

            public void addResult(ResultClass resultClass) {
                SQLiteDatabase db = databasehelp.getWritableDatabase();
                ContentValues values = new ContentValues();
                //values.put(KEY_RESID, resultClass.getId());
                values.put(KEY_RESUSECOD, resultClass.getUserCode());
                values.put(KEY_RESEVAID, resultClass.getEvaluationId());
                values.put(KEY_RESEVANAM, resultClass.getEvaluationName());
                values.put(KEY_RESQUEID, resultClass.getQuestionId());
                values.put(KEY_RESQUENAM, resultClass.getQuestionName());
                values.put(KEY_RESPERCOD, resultClass.getPersonalCode());
                values.put(KEY_RESPERNAM, resultClass.getPersonalName());
                values.put(KEY_RESPERDOC, resultClass.getPersonalDocument());
                values.put(KEY_RESANS, resultClass.getAnswer());
                values.put(KEY_RESDAT, resultClass.getDate());
                values.put(KEY_RESHOU, resultClass.getHour());
                values.put(KEY_RESLOA, resultClass.getLoad());
                db.insert(TABLE_APP_RESULT, null, values);
                db.close();
                Log.i("DB Result", "Created successfull");
            }

            public void updateResult(ResultClass resultClass, String questionId) {
                SQLiteDatabase db = databasehelp.getWritableDatabase();
                ContentValues values = new ContentValues();
                //values.put(KEY_RESID, resultClass.getId());
                values.put(KEY_RESUSECOD, resultClass.getUserCode());
                values.put(KEY_RESEVAID, resultClass.getEvaluationId());
                values.put(KEY_RESEVANAM, resultClass.getEvaluationName());
                values.put(KEY_RESQUEID, resultClass.getQuestionId());
                values.put(KEY_RESQUENAM, resultClass.getQuestionName());
                values.put(KEY_RESPERCOD, resultClass.getPersonalCode());
                values.put(KEY_RESPERNAM, resultClass.getPersonalName());
                values.put(KEY_RESPERDOC, resultClass.getPersonalDocument());
                values.put(KEY_RESANS, resultClass.getAnswer());
                values.put(KEY_RESDAT, resultClass.getDate());
                values.put(KEY_RESHOU, resultClass.getHour());
                values.put(KEY_RESLOA, resultClass.getLoad());
                db.update(TABLE_APP_RESULT, values, KEY_RESQUEID + " = ?",new String[] { questionId});
                db.close();
                Log.i("DB Result", "Updated successfull");

            }
            public void updateResult(String questionId, String date, String hour, String answer) {
                SQLiteDatabase db = databasehelp.getWritableDatabase();
                ContentValues values = new ContentValues();
                //values.put(KEY_RESID, resultClass.getId());
                values.put(KEY_RESANS, answer);
                values.put(KEY_RESDAT, date);
                values.put(KEY_RESHOU, hour);
                db.update(TABLE_APP_RESULT, values, KEY_RESQUEID + " = ?", new String[]{questionId});
                db.close();
                Log.i("DB Result", "Updated successfull");

            }

            public ArrayList<ResultClass> getPendingResult() {
                ArrayList<ResultClass> loadResult = new ArrayList<ResultClass>();
                String selectQuery = "SELECT * FROM " + TABLE_APP_RESULT + " WHERE "+ KEY_RESLOA + "='0'";
                SQLiteDatabase db = databasehelp.getWritableDatabase();
                Cursor cursor = db.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    do {
                        ResultClass resultClass = new ResultClass();
                        resultClass.setId(cursor.getInt(cursor.getColumnIndex(KEY_RESID)));
                        resultClass.setUserCode(cursor.getString(cursor.getColumnIndex(KEY_RESUSECOD)));
                        resultClass.setEvaluationId(cursor.getString(cursor.getColumnIndex(KEY_RESEVAID)));
                        resultClass.setEvaluationName(cursor.getString(cursor.getColumnIndex(KEY_RESEVANAM)));
                        resultClass.setQuestionId(cursor.getString(cursor.getColumnIndex(KEY_RESQUEID)));
                        resultClass.setQuestionName(cursor.getString(cursor.getColumnIndex(KEY_RESQUENAM)));
                        resultClass.setPersonalCode(cursor.getString(cursor.getColumnIndex(KEY_RESPERCOD)));
                        resultClass.setPersonalName(cursor.getString(cursor.getColumnIndex(KEY_RESPERNAM)));
                        resultClass.setPersonalDocument(cursor.getString(cursor.getColumnIndex(KEY_RESPERDOC)));
                        resultClass.setAnswer(cursor.getString(cursor.getColumnIndex(KEY_RESANS)));
                        resultClass.setDate(cursor.getString(cursor.getColumnIndex(KEY_RESDAT)));
                        resultClass.setHour(cursor.getString(cursor.getColumnIndex(KEY_RESHOU)));
                        resultClass.setLoad(cursor.getString(cursor.getColumnIndex(KEY_RESLOA)));
                        loadResult.add(resultClass);
                    } while (cursor.moveToNext());
                }
                db.close();
                return loadResult;
            }

            public ArrayList<ResultClass> getResultData() {
                ArrayList<ResultClass> loadResult = new ArrayList<ResultClass>();
                String selectQuery = "SELECT * FROM " + TABLE_APP_RESULT;
                SQLiteDatabase db = databasehelp.getWritableDatabase();
                Cursor cursor = db.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    do {
                        ResultClass resultClass = new ResultClass();
                        resultClass.setId(cursor.getInt(cursor.getColumnIndex(KEY_RESID)));
                        resultClass.setUserCode(cursor.getString(cursor.getColumnIndex(KEY_RESUSECOD)));
                        resultClass.setEvaluationId(cursor.getString(cursor.getColumnIndex(KEY_RESEVAID)));
                        resultClass.setEvaluationName(cursor.getString(cursor.getColumnIndex(KEY_RESEVANAM)));
                        resultClass.setQuestionId(cursor.getString(cursor.getColumnIndex(KEY_RESQUEID)));
                        resultClass.setQuestionName(cursor.getString(cursor.getColumnIndex(KEY_RESQUENAM)));
                        resultClass.setPersonalCode(cursor.getString(cursor.getColumnIndex(KEY_RESPERCOD)));
                        resultClass.setPersonalName(cursor.getString(cursor.getColumnIndex(KEY_RESPERNAM)));
                        resultClass.setPersonalDocument(cursor.getString(cursor.getColumnIndex(KEY_RESPERDOC)));
                        resultClass.setAnswer(cursor.getString(cursor.getColumnIndex(KEY_RESANS)));
                        resultClass.setDate(cursor.getString(cursor.getColumnIndex(KEY_RESDAT)));
                        resultClass.setHour(cursor.getString(cursor.getColumnIndex(KEY_RESHOU)));
                        resultClass.setLoad(cursor.getString(cursor.getColumnIndex(KEY_RESLOA)));
                        loadResult.add(resultClass);
                    } while (cursor.moveToNext());
                }
                db.close();
                return loadResult;
            }

            public ResultClass getResult(int questionId) {
                SQLiteDatabase db = databasehelp.getReadableDatabase();
                Cursor cursor = db.query(TABLE_APP_RESULT, new String[] {KEY_RESID, KEY_RESUSECOD,
                                KEY_RESEVAID, KEY_RESEVANAM, KEY_RESQUEID, KEY_RESQUENAM, KEY_RESPERCOD, KEY_RESPERNAM, KEY_RESPERDOC,
                                KEY_RESANS, KEY_RESDAT, KEY_RESHOU, KEY_RESLOA}, KEY_RESQUEID + "=?",
                        new String[] { String.valueOf(questionId) }, null, null, null, null);
                if (cursor != null){
                    cursor.moveToFirst();
                }

                ResultClass resultClass = new ResultClass(cursor.getInt(0), cursor.getString(1), cursor.getString(2),
                        cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getString(6),
                        cursor.getString(7), cursor.getString(8), cursor.getString(9), cursor.getString(10), cursor.getString(11), cursor.getString(12));
                db.close();
                return resultClass;
            }

            public String getResultAnswer(String questionId) {
                String result = "";
                SQLiteDatabase db = databasehelp.getReadableDatabase();
                Cursor cursor = db.query(TABLE_APP_RESULT, new String[]{KEY_RESANS}, KEY_RESQUEID + "=?",
                        new String[]{String.valueOf(questionId)}, null, null, null, null);
                if (cursor.moveToFirst()) {
                    result = cursor.getString((cursor.getColumnIndex(KEY_RESANS)));
                }
                cursor.close();
                db.close();
                return result;
            }

            public void updateResultLoad(String questionId, String load) {
                SQLiteDatabase db = databasehelp.getWritableDatabase();
                ContentValues values = new ContentValues();
                values.put(KEY_RESLOA, load);
                db.update(TABLE_APP_RESULT, values, KEY_RESQUEID + " = ?",new String[] { questionId});
                db.close();
            }

            public String getData(int nField, String sName) {
                String _data = null;
                SQLiteDatabase db = databasehelp.getReadableDatabase();
                Cursor cursor = db.query(TABLE_APP_RESULT, new String[] {KEY_RESID, KEY_RESUSECOD,
                                KEY_RESEVAID, KEY_RESEVANAM, KEY_RESQUEID, KEY_RESQUENAM, KEY_RESPERCOD, KEY_RESPERNAM, KEY_RESPERDOC,
                                KEY_RESANS, KEY_RESDAT, KEY_RESHOU, KEY_RESLOA}, KEY_RESQUEID + "=?",
                        new String[] {sName}, null, null, null, null);
                if (cursor != null){
                    cursor.moveToFirst();
                }
                _data = cursor.getString(nField);
                db.close();
                return _data;
            }
        }

        public class AppLoadImageSql{
            public AppLoadImageSql() {}

            /** Borrar imagenes despues del envio, todas. */
            public void deleteLoadImage(){
                SQLiteDatabase db = databasehelp.getWritableDatabase();
                db.delete(TABLE_APP_LOAD_IMAGE,null,null);
                db.close();
            }
            public void deleteLoadImageById(String id){
                SQLiteDatabase db = databasehelp.getWritableDatabase();
                db.delete(TABLE_APP_LOAD_IMAGE,KEY_LOAIMAID + " = ?",new String[] { id});
                db.close();
            }

            public int countElements(){
                int result = 0;
                SQLiteDatabase db = databasehelp.getWritableDatabase();
                Cursor mCount= db.rawQuery("SELECT COUNT(*) FROM " + TABLE_APP_LOAD_IMAGE,null);
                mCount.moveToFirst();
                result= mCount.getInt(0);
                db.close();
                return result;
            }

            public void addLoadImage(LoadImageClass loadImageClass) {
                SQLiteDatabase db = databasehelp.getWritableDatabase();
                ContentValues values = new ContentValues();
                //values.put(KEY_LOAIMAID, loadImageClass.getId());
                values.put(KEY_LOAIMAUSECOD, loadImageClass.getUserCode());
                values.put(KEY_LOAIMAEVAID, loadImageClass.getEvaluationId());
                values.put(KEY_LOAIMAEVANAM, loadImageClass.getEvaluationName());
                values.put(KEY_LOAIMAPERCOD, loadImageClass.getPersonalCode());
                values.put(KEY_LOAIMAPERNAM, loadImageClass.getPersonalName());
                values.put(KEY_LOAIMAPERDOC, loadImageClass.getPersonalDocument());
                values.put(KEY_LOAIMADAT, loadImageClass.getDate());
                values.put(KEY_LOAIMAHOU, loadImageClass.getHour());
                values.put(KEY_LOAIMANAMPHO, loadImageClass.getNamePhoto());
                values.put(KEY_LOAIMAPHO, loadImageClass.getPhoto());
                values.put(KEY_LOAIMAOBS, loadImageClass.getObservation());
                values.put(KEY_LOAIMALOA, loadImageClass.getLoad());
                db.insert(TABLE_APP_LOAD_IMAGE, null, values);
                db.close();
            }

            public ArrayList<LoadImageClass> getPendingLoadImageData() {
                ArrayList<LoadImageClass> loadImageList = new ArrayList<LoadImageClass>();
                String selectQuery = "SELECT * FROM " + TABLE_APP_LOAD_IMAGE + " WHERE " + KEY_LOAIMALOA + "='0'";
                SQLiteDatabase db = databasehelp.getWritableDatabase();
                Cursor cursor = db.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    do {
                        LoadImageClass image = new LoadImageClass();
                        image.setId(cursor.getInt(cursor.getColumnIndex(KEY_LOAIMAID)));
                        image.setUserCode(cursor.getString(cursor.getColumnIndex(KEY_LOAIMAUSECOD)));
                        image.setEvaluationId(cursor.getString(cursor.getColumnIndex(KEY_LOAIMAEVAID)));
                        image.setEvaluationName(cursor.getString(cursor.getColumnIndex(KEY_LOAIMAEVANAM)));
                        image.setPersonalCode(cursor.getString(cursor.getColumnIndex(KEY_LOAIMAPERCOD)));
                        image.setPersonalName(cursor.getString(cursor.getColumnIndex(KEY_LOAIMAPERNAM)));
                        image.setPersonalDocument(cursor.getString(cursor.getColumnIndex(KEY_LOAIMAPERDOC)));
                        image.setDate(cursor.getString(cursor.getColumnIndex(KEY_LOAIMADAT)));
                        image.setHour(cursor.getString(cursor.getColumnIndex(KEY_LOAIMAHOU)));
                        image.setNamePhoto(cursor.getString(cursor.getColumnIndex(KEY_LOAIMANAMPHO)));
                        image.setPhoto(cursor.getString(cursor.getColumnIndex(KEY_LOAIMAPHO)));
                        image.setObservation(cursor.getString(cursor.getColumnIndex(KEY_LOAIMAOBS)));
                        image.setLoad(cursor.getString(cursor.getColumnIndex(KEY_LOAIMALOA)));
                        loadImageList.add(image);
                    } while (cursor.moveToNext());
                }
                db.close();
                return loadImageList;
            }

            public ArrayList<LoadImageClass> getLoadImageData() {
                ArrayList<LoadImageClass> loadImageList = new ArrayList<LoadImageClass>();
                String selectQuery = "SELECT * FROM " + TABLE_APP_LOAD_IMAGE;
                SQLiteDatabase db = databasehelp.getWritableDatabase();
                Cursor cursor = db.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    do {
                        LoadImageClass image = new LoadImageClass();
                        image.setId(cursor.getInt(cursor.getColumnIndex(KEY_LOAIMAID)));
                        image.setUserCode(cursor.getString(cursor.getColumnIndex(KEY_LOAIMAUSECOD)));
                        image.setEvaluationId(cursor.getString(cursor.getColumnIndex(KEY_LOAIMAEVAID)));
                        image.setEvaluationName(cursor.getString(cursor.getColumnIndex(KEY_LOAIMAEVANAM)));
                        image.setPersonalCode(cursor.getString(cursor.getColumnIndex(KEY_LOAIMAPERCOD)));
                        image.setPersonalName(cursor.getString(cursor.getColumnIndex(KEY_LOAIMAPERNAM)));
                        image.setPersonalDocument(cursor.getString(cursor.getColumnIndex(KEY_LOAIMAPERDOC)));
                        image.setDate(cursor.getString(cursor.getColumnIndex(KEY_LOAIMADAT)));
                        image.setHour(cursor.getString(cursor.getColumnIndex(KEY_LOAIMAHOU)));
                        image.setNamePhoto(cursor.getString(cursor.getColumnIndex(KEY_LOAIMANAMPHO)));
                        image.setPhoto(cursor.getString(cursor.getColumnIndex(KEY_LOAIMAPHO)));
                        image.setObservation(cursor.getString(cursor.getColumnIndex(KEY_LOAIMAOBS)));
                        image.setLoad(cursor.getString(cursor.getColumnIndex(KEY_LOAIMALOA)));
                        loadImageList.add(image);
                    } while (cursor.moveToNext());
                }
                db.close();
                return loadImageList;
            }

            public LoadImageClass getLoadImage(int nId) {
                SQLiteDatabase db = databasehelp.getReadableDatabase();
                Cursor cursor = db.query(TABLE_APP_LOAD_IMAGE , new String[] {KEY_LOAIMAID, KEY_LOAIMAUSECOD,
                                KEY_LOAIMAEVAID, KEY_LOAIMAEVANAM, KEY_LOAIMAPERCOD, KEY_LOAIMAPERNAM, KEY_LOAIMAPERDOC, KEY_LOAIMADAT,
                                KEY_LOAIMAHOU, KEY_LOAIMANAMPHO,KEY_LOAIMAPHO,  KEY_LOAIMAOBS, KEY_LOAIMALOA}, KEY_LOAIMAID + "=?",
                        new String[] { String.valueOf(nId) }, null, null, null, null);
                if (cursor != null){
                    cursor.moveToFirst();
                }

                LoadImageClass image = new LoadImageClass(cursor.getInt(0), cursor.getString(1), cursor.getString(2),
                        cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getString(6),
                cursor.getString(7), cursor.getString(8), cursor.getString(9), cursor.getString(10),
                        cursor.getString(11), cursor.getString(12));
                db.close();
                return image;
            }

            public void updateLoadImageClass(String id, String load) {
                SQLiteDatabase db = databasehelp.getWritableDatabase();
                ContentValues values = new ContentValues();
                values.put(KEY_LOAIMADAT, load);
                db.update(TABLE_APP_LOAD_IMAGE, values, KEY_LOAIMAID + " = ?",new String[] { id});
                db.close();
            }

            public String getData(int nField, String sName) {
                String _data = null;
                SQLiteDatabase db = databasehelp.getReadableDatabase();
                Cursor cursor = db.query(TABLE_APP_LOAD_IMAGE , new String[] {KEY_LOAIMAID, KEY_LOAIMAUSECOD,
                                KEY_LOAIMAEVAID, KEY_LOAIMAEVANAM, KEY_LOAIMAPERCOD, KEY_LOAIMAPERNAM, KEY_LOAIMAPERDOC, KEY_LOAIMADAT,
                                KEY_LOAIMAHOU, KEY_LOAIMANAMPHO,KEY_LOAIMAPHO,  KEY_LOAIMAOBS, KEY_LOAIMALOA}, KEY_LOAIMAID + "=?",
                        new String[] {sName}, null, null, null, null);
                if (cursor != null){
                    cursor.moveToFirst();
                }
                _data = cursor.getString(nField);
                db.close();
                return _data;
            }
        }

        public class AppSynchronizationSql {
            public AppSynchronizationSql() {}

            public void deleteSynchronization(){
                SQLiteDatabase db = databasehelp.getWritableDatabase();
                db.delete(TABLE_APP_SYNCHRONIZATION,null,null);
                db.close();
            }

            public boolean checkIfExists(String classType){
                int result = 0;
                SQLiteDatabase db = databasehelp.getWritableDatabase();
                Cursor mCount= db.rawQuery("SELECT COUNT(*) FROM " + TABLE_APP_SYNCHRONIZATION + " WHERE as_cla_nam='" + classType + "'",null);
                mCount.moveToFirst();
                result= mCount.getInt(0);
                db.close();
                return result > 0;
            }

            public String getDateLastSyncStock() {
                String selectQuery = "SELECT * FROM " + TABLE_APP_SYNCHRONIZATION + " WHERE as_cla_nam='Stock'";
                SQLiteDatabase db = databasehelp.getWritableDatabase();
                Cursor cursor = db.rawQuery(selectQuery, null);
                if(cursor.moveToLast()){
                    return cursor.getString(cursor.getColumnIndex(KEY_SYNDATHOUEND));
                }
                db.close();
                return "";
            }

            public void addSynchronization(SynchronizationClass synchronizationClass) {

                SQLiteDatabase db = databasehelp.getWritableDatabase();
                ContentValues values = new ContentValues();
                //values.put(KEY_SYNID, synchronizationClass.getId());
                values.put(KEY_SYNCLANAM, synchronizationClass.getClassName());
                values.put(KEY_SYNDATHOUSTA, synchronizationClass.getDateHourStart());
                values.put(KEY_SYNDATHOUEND, synchronizationClass.getDateHourEnd());
                values.put(KEY_SYNREGNUM, synchronizationClass.getRegisterNumber());
                values.put(KEY_SYNFAINUM, synchronizationClass.getFailureNumber());
                values.put(KEY_SYNSTA, synchronizationClass.getState());

                db.insert(TABLE_APP_SYNCHRONIZATION, null, values);
                db.close();
            }


            public ArrayList<SynchronizationClass> getSynchronizationData() {
                ArrayList<SynchronizationClass> synchronizationClassList = new ArrayList<SynchronizationClass>();
                String selectQuery = "SELECT * FROM " + TABLE_APP_SYNCHRONIZATION;
                SQLiteDatabase db = databasehelp.getWritableDatabase();
                Cursor cursor = db.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    do {
                        SynchronizationClass synchronizationClass = new SynchronizationClass();
                        synchronizationClass.setId(cursor.getInt(cursor.getColumnIndex(KEY_SYNID)));
                        synchronizationClass.setClassName(cursor.getString(cursor.getColumnIndex(KEY_SYNCLANAM)));
                        synchronizationClass.setDateHourStart(cursor.getString(cursor.getColumnIndex(KEY_SYNDATHOUSTA)));
                        synchronizationClass.setDateHourEnd(cursor.getString(cursor.getColumnIndex(KEY_SYNDATHOUEND)));
                        synchronizationClass.setRegisterNumber(cursor.getString(cursor.getColumnIndex(KEY_SYNREGNUM)));
                        synchronizationClass.setFailureNumber(cursor.getString(cursor.getColumnIndex(KEY_SYNFAINUM)));
                        synchronizationClass.setState(cursor.getString(cursor.getColumnIndex(KEY_SYNSTA)));

                        synchronizationClassList.add(synchronizationClass);
                    } while (cursor.moveToNext());
                }
                db.close();
                return synchronizationClassList;
            }



            public SynchronizationClass getSynchronization(String nId) {
                SQLiteDatabase db = databasehelp.getReadableDatabase();
                Cursor cursor = db.query(TABLE_APP_SYNCHRONIZATION, new String[] {KEY_SYNID, KEY_SYNCLANAM, KEY_SYNDATHOUSTA, KEY_SYNDATHOUEND,
                                KEY_SYNREGNUM, KEY_SYNFAINUM, KEY_SYNSTA}, KEY_SYNID + "=?",
                        new String[] { String.valueOf(nId) }, null, null, null, null);
                if (cursor != null){
                    cursor.moveToFirst();
                }

                SynchronizationClass synchronizationClass = new SynchronizationClass(cursor.getInt(0), cursor.getString(1), cursor.getString(2),
                        cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getString(6));
                db.close();
                return synchronizationClass;
            }

            public String getData(int nField, String sName) {
                String _data = null;
                SQLiteDatabase db = databasehelp.getReadableDatabase();
                Cursor cursor = db.query(TABLE_APP_SYNCHRONIZATION, new String[] {KEY_SYNID, KEY_SYNCLANAM, KEY_SYNDATHOUSTA, KEY_SYNDATHOUEND,
                                KEY_SYNREGNUM, KEY_SYNFAINUM, KEY_SYNSTA}, KEY_SYNID + "=?",
                        new String[] {sName}, null, null, null, null);
                if (cursor != null){
                    cursor.moveToFirst();
                }
                _data = cursor.getString(nField);
                db.close();
                return _data;
            }

        }

    }

}