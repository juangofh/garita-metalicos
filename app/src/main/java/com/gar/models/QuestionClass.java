package com.gar.models;

public class QuestionClass {

	private int id;
	private String name;
	private String evaluationID;
	private String order;
	private String type;
	private String parameters;
	private String alternatives;
	private String active;


	public QuestionClass(){}
	public QuestionClass(int id, String name, String evaluationID, String order, String type, String parameters, String alternatives, String active) {
		this.id = id;
		this.name = name;
		this.evaluationID = evaluationID;
		this.order = order;
		this.active = active;
		this.parameters = parameters;
		this.type = type;
		this.alternatives = alternatives;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEvaluationID() {
		return evaluationID;
	}

	public void setEvaluationID(String evaluationID) {
		this.evaluationID = evaluationID;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getParameters() { return parameters; }

	public void setParameters(String parameters) { this.parameters = parameters; }

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getAlternatives() {
		return alternatives;
	}

	public void setAlternatives(String alternatives) {
		this.alternatives = alternatives;
	}
}
