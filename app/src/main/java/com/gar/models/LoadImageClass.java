package com.gar.models;

public class LoadImageClass {

    private int id;
    private String userCode;
    private String evaluationId;
    private String evaluationName;
    private String personalCode;
    private String personalName;
    private String personalDocument;
    private String date;
    private String hour;
    private String namePhoto;
    private String photo;
    private String observation;
    private String load;

    public LoadImageClass(){}

    public LoadImageClass(int id, String userCode, String evaluationId, String evaluationName, String personalCode, String personalName, String personalDocument, String date, String hour, String namePhoto, String photo, String observation, String load) {
        this.id = id;
        this.userCode = userCode;
        this.evaluationId = evaluationId;
        this.evaluationName = evaluationName;
        this.personalCode = personalCode;
        this.personalName = personalName;
        this.personalDocument = personalDocument;
        this.date = date;
        this.hour = hour;
        this.namePhoto = namePhoto;
        this.photo = photo;
        this.observation = observation;
        this.load = load;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getEvaluationId() {
        return evaluationId;
    }

    public void setEvaluationId(String evaluationId) {
        this.evaluationId = evaluationId;
    }

    public String getEvaluationName() {
        return evaluationName;
    }

    public void setEvaluationName(String evaluationName) {
        this.evaluationName = evaluationName;
    }

    public String getPersonalCode() {
        return personalCode;
    }

    public void setPersonalCode(String personalCode) {
        this.personalCode = personalCode;
    }

    public String getPersonalName() {
        return personalName;
    }

    public void setPersonalName(String personalName) {
        this.personalName = personalName;
    }

    public String getPersonalDocument() {
        return personalDocument;
    }

    public void setPersonalDocument(String personalDocument) {
        this.personalDocument = personalDocument;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public String getNamePhoto() {
        return namePhoto;
    }

    public void setNamePhoto(String namePhoto) {
        this.namePhoto = namePhoto;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public String getLoad() {
        return load;
    }

    public void setLoad(String load) {
        this.load = load;
    }
}
