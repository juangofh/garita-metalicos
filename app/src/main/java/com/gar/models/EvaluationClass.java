package com.gar.models;

public class EvaluationClass {

    public int id;
    public String name;
    public String startDate;
    public String endDate;
    public String app;
    public String link;
    public String type; /* Type to set to the OT*/
    public String hour;
    public String limit;
    public String active;

    public EvaluationClass(){}
    public EvaluationClass(int id, String name, String startDate, String endDate, String app, String link, String type, String hour, String limit , String active) {
        this.id = id;
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.app = app;
        this.link = link;
        this.type = type;
        this.hour = hour;
        this.limit = type;
        this.active = active;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getApp() {
        return app;
    }

    public void setApp(String app) {
        this.app = app;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }
}
