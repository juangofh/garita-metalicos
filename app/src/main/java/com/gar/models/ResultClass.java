package com.gar.models;

public class ResultClass {

    private int id;
    private String userCode;
    private String evaluationId;
    private String evaluationName;
    private String questionId;
    private String questionName;
    private String personalCode;
    private String personalName;
    private String personalDocument;
    private String answer;              //always YES here || E-learning can have text or the selection
    private String date;
    private String hour;
    private String load;

    public ResultClass(){}

    public ResultClass(int id, String userCode, String evaluationId, String evaluationName, String questionId, String questionName, String personalCode, String personalName, String personalDocument, String answer, String date, String hour, String load) {
        this.id = id;
        this.userCode = userCode;
        this.evaluationId = evaluationId;
        this.evaluationName = evaluationName;
        this.questionId = questionId;
        this.questionName = questionName;
        this.personalCode = personalCode;
        this.personalName = personalName;
        this.personalDocument = personalDocument;
        this.answer = answer;
        this.date = date;
        this.hour = hour;
        this.load = load;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getEvaluationId() {
        return evaluationId;
    }

    public void setEvaluationId(String evaluationId) {
        this.evaluationId = evaluationId;
    }

    public String getEvaluationName() {
        return evaluationName;
    }

    public void setEvaluationName(String evaluationName) {
        this.evaluationName = evaluationName;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public String getQuestionName() {
        return questionName;
    }

    public void setQuestionName(String questionName) {
        this.questionName = questionName;
    }

    public String getPersonalCode() {
        return personalCode;
    }

    public void setPersonalCode(String personalCode) {
        this.personalCode = personalCode;
    }

    public String getPersonalName() {
        return personalName;
    }

    public void setPersonalName(String personalName) {
        this.personalName = personalName;
    }

    public String getPersonalDocument() {
        return personalDocument;
    }

    public void setPersonalDocument(String personalDocument) {
        this.personalDocument = personalDocument;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public String getLoad() {
        return load;
    }

    public void setLoad(String load) {
        this.load = load;
    }
}
