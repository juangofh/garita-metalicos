package com.gar.models;

public class OTClass {

    private int id;
    private String otCode; /* This makes unique my entry for otclass*/
    private String lastname;
    private String firstname;
    private String license;
    private String plate;
    private String typeVehicle;
    private String typeService;  /* Evaluation type */
    private String otSap;
    private String evaluationID;
    private String date;

    public OTClass(){}

    public OTClass(int id, String otCode, String lastname, String firstname, String license, String plate, String typeVehicle, String typeService, String otSap, String evaluationID, String date) {
        this.id = id;
        this.otCode = otCode;
        this.lastname = lastname;
        this.firstname = firstname;
        this.license = license;
        this.plate = plate;
        this.typeVehicle = typeVehicle;
        this.typeService = typeService;
        this.otSap = otSap;
        this.evaluationID = evaluationID;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOtCode() {
        return otCode;
    }

    public void setOtCode(String otCode) {
        this.otCode = otCode;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public String getTypeVehicle() {
        return typeVehicle;
    }

    public void setTypeVehicle(String typeVehicle) {
        this.typeVehicle = typeVehicle;
    }

    public String getTypeService() {
        return typeService;
    }

    public void setTypeService(String typeService) {
        this.typeService = typeService;
    }

    public String getOtSap() {
        return otSap;
    }

    public void setOtSap(String otSap) {
        this.otSap = otSap;
    }

    public String getEvaluationID() {
        return evaluationID;
    }

    public void setEvaluationID(String evaluationID) {
        this.evaluationID = evaluationID;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
